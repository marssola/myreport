QT += gui qml quick quickcontrols2 core sql network
CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

HEADERS += \
    src/utils/statusbar/statusbar.h \
    src/utils/statusbar/statusbar_p.h \
    src/utils/share/share.h \
    src/utils/download/downloadmanager.h

SOURCES += \
        main.cpp \
        src/utils/statusbar/statusbar.cpp \
        src/utils/download/downloadmanager.cpp

android {
    QT += androidextras
    SOURCES += src/utils/statusbar/statusbar_android.cpp \
               src/utils/share/share_android.cpp

    OTHER_FILES += android/src/com/utils/QShareUtils.java

    contains(ANDROID_TARGET_ARCH,armeabi-v7a) {
        ANDROID_EXTRA_LIBS = \
            $$PWD/libs/libcrypto.so \
            $$PWD/libs/libssl.so
    }

    DISTFILES += \
        android/AndroidManifest.xml \
        android/res/values/libs.xml
    ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android
} else:ios {
    LIBS += -framework UIKit
    OBJECTIVE_SOURCES += src/utils/statusbar/statusbar_ios.mm \
                         src/utils/share/share_ios.mm
} else {
    SOURCES += src/utils/statusbar/statusbar_dummy.cpp \
               src/utils/share/share.cpp
}

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    ui/functions/Database.js

VERSION = 1.0
DEFINES += GIT_VERSION="\\\"$$system(git --git-dir $$PWD/.git --work-tree $$PWD describe --always --tags)\\\""
