#include "share.h"
#include <QtAndroid>
#include <QtAndroidExtras/QAndroidJniObject>

Share::Share(QQuickItem *parent) : QQuickItem(parent)
{
}

void Share::share(const QString &content)
{
    QAndroidJniObject jsText = QAndroidJniObject::fromString(content);
    QAndroidJniObject::callStaticMethod<void>("com/utils/QShareUtils",
                                              "share",
                                              "(Ljava/lang/String;)V",
                                              jsText.object<jstring>());
}
