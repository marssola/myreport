#ifndef SHAREUTILS_H
#define SHAREUTILS_H

#include <QQuickItem>
#include <QString>

class Share : public QQuickItem
{
    Q_OBJECT

public:
    explicit Share(QQuickItem *parent = nullptr);
    Q_INVOKABLE void share(const QString &content);

Q_SIGNALS:
    void shareContent();
};

#endif // SHAREUTILS_H
