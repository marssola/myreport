#import "share.h"
#import <UIKit/UIKit.h>
#import <QGuiApplication>
#import <QQuickWindow>

Share::Share(QQuickItem *parent) : QQuickItem(parent)
{
}

void Share::share(const QString &content)
{
    NSMutableArray *sharingItems = [NSMutableArray new];

    if (!content.isEmpty())
        [sharingItems addObject:content.toNSString()];

    // get the main window rootViewController
    UIViewController *qtController = [[UIApplication sharedApplication].keyWindow rootViewController];

    UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
    if ( [activityController respondsToSelector:@selector(popoverPresentationController)] ) { // iOS8
        activityController.popoverPresentationController.sourceView = qtController.view;
    }
    [qtController presentViewController:activityController animated:YES completion:nil];
}
