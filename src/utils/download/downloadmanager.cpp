#include "downloadmanager.h"
#include <QString>
#include <QFileInfo>
#include <QDir>
#include <QRegularExpression>
#include <QRegularExpressionMatch>
#include <QStandardPaths>
#include <QDebug>

DownloadManager::DownloadManager()
{
    connect(this, &DownloadManager::startNextDownloadChanged, &DownloadManager::startNextDownload);
}

void DownloadManager::addItemDownload(const QJsonObject object)
{
    if (object.contains("id") && object.contains("url"))
    {
        m_list_items_download.push_back(object);
        if (!m_downloading)
            emit startNextDownloadChanged();
    }
}

void DownloadManager::removeFile(const QString filename)
{
    QFile file(filename);
    if (file.exists())
        file.remove();
    file.close();
}

QString DownloadManager::savePathFile(const QUrl &url)
{
    QString getPath = QString("./%1").arg(appPath);
    QString path = url.path();
    QFileInfo file = QFileInfo(path);

    QMimeDatabase dbMime;
    QString mime = dbMime.mimeTypeForFile(file.fileName()).name();

    QRegularExpression regexImage("image");
    QRegularExpressionMatch regexImageMatch = regexImage.match(mime);
    if (regexImageMatch.hasMatch())
        getPath = QString("%1/%2").arg(QStandardPaths::writableLocation(QStandardPaths::PicturesLocation)).arg(appPath);
    else
        getPath = QString("%1/%2").arg(QStandardPaths::writableLocation(QStandardPaths::DownloadLocation)).arg(appPath);

    QDir dir = QDir(getPath);
    if (!dir.exists())
        dir.mkdir(getPath);

    return  QString("%1/%2").arg(getPath).arg(file.fileName());
}

QString DownloadManager::saveFileName(const QUrl &url)
{
    QString path = savePathFile(url);
    QFileInfo file = QFileInfo(path);
    QString basename = file.baseName();

    QMimeDatabase dbMime;
    QString mime = dbMime.mimeTypeForFile(file.fileName()).name();

    if (basename.isEmpty())
        basename = "download";

    if (QFile::exists(QString("%1/%2.%3").arg(file.path()).arg(basename).arg(file.suffix())))
    {
        int i = 1;
        basename += '.';
        while (QFile::exists(QString("%1/%2%3.%4").arg(file.path()).arg(basename).arg(QString::number(i)).arg(file.suffix())))
            ++i;
        basename += QString::number(i);
    }

    return QString("%1/%2.%3").arg(file.path()).arg(basename).arg(file.suffix());
}

void DownloadManager::startNextDownload()
{
    if (!m_downloading && !m_list_items_download.isEmpty())
    {
        m_downloading = true;

        QJsonObject object = m_list_items_download.at(0).toObject();
        QUrl url = object.value("url").toString();
        QString filename = saveFileName(url);
        output.setFileName(filename);

        if (!output.open(QIODevice::WriteOnly))
        {
            qDebug() << "Problem opening save file " << qPrintable(filename) << " for download " << url.toEncoded().constData() << ": " << output.errorString();
            m_list_items_download.pop_back();
            startNextDownload();
            return;
        }

        QNetworkRequest request(url);
        currentDownload = manager.get(request);
        connect(currentDownload, SIGNAL(downloadProgress(qint64, qint64)), SLOT(downloadProgress(qint64, qint64)));
        connect(currentDownload, SIGNAL(readyRead()), SLOT(downloadReadyRead()));
        connect(currentDownload, SIGNAL(finished()), SLOT(downloadFinished()));

        downloadTime.start();

        object.insert("downloading", 0.0);
        object.insert("file", "");
        m_list_download = object;
        emit listDownloadChanged();
    }
}

void DownloadManager::downloadProgress(qint64 bytesReceived, qint64 bytesTotal)
{
    QString unit;
    QJsonObject object = m_list_items_download.at(0).toObject();

    double downloading = (bytesReceived / bytesTotal);
    double speed = bytesReceived * (1000.0 / downloadTime.elapsed());
    if (speed < 1024)
        unit = "b/s";
    else if (speed < 1024 * 1024)
    {
        speed /= 1024;
        unit = "KB/s";
    }
    else
    {
        speed /= 1024 * 1024;
        unit = "MB/s";
    }

    object.insert("downloading", downloading);
    object.insert("speed", QString("%1 %2").arg(speed).arg(unit));
    m_list_download = object;
    emit listDownloadChanged();
}


void DownloadManager::downloadReadyRead()
{
    output.write(currentDownload->readAll());
}

void DownloadManager::downloadFinished()
{
    output.close();
    if (currentDownload->error())
    {
        qDebug() << "Failed: " << qPrintable(currentDownload->errorString());
        output.remove();
    }
    else
    {
        QJsonObject object = m_list_items_download.at(0).toObject();
        object.insert("file", output.fileName());
        m_list_download = object;
        emit listDownloadChanged();
    }

    currentDownload->deleteLater();
    m_list_items_download.pop_front();
    m_downloading = false;
    emit startNextDownloadChanged();
}
