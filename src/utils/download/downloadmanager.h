#ifndef DOWNLOADMANAGER_H
#define DOWNLOADMANAGER_H

#include <QQuickItem>
#include <QtNetwork>
#include <QJsonObject>
#include <QJsonArray>

class DownloadManager : public QQuickItem
{
    Q_OBJECT
    Q_PROPERTY(QJsonObject listDownload READ listDownload NOTIFY listDownloadChanged)

public:
    DownloadManager();
    Q_INVOKABLE void addItemDownload(const QJsonObject object);
    Q_INVOKABLE void removeFile(const QString filename);
    QString savePathFile(const QUrl &url);
    QString saveFileName(const QUrl &url);

signals:
    void listDownloadChanged();
    void startNextDownloadChanged();

private slots:
    void startNextDownload();
    void downloadProgress(qint64 bytesReceived, qint64 bytesTotal);
    void downloadReadyRead();
    void downloadFinished();

private:
    bool m_downloading = false;

    QJsonArray m_list_items_download;
    QJsonObject m_list_download = {};
    inline QJsonObject listDownload() { return m_list_download; }

    QNetworkAccessManager manager;
    QNetworkReply *currentDownload = nullptr;
    QFile output;
    QTime downloadTime;

    QString appPath = "MyReport";
};

#endif // DOWNLOADMANAGER_H
