import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.4
import Qt.labs.settings 1.0
import QtQuick.LocalStorage 2.0

import com.Utils.Share 0.1
import com.Utils.StatusBar 0.1
import com.Utils.DownloadManager 0.1

import "./ui/functions/Utils.js" as Utils
import "./ui/functions/Database.js" as DB
import "./ui/navigation"
import "./ui/Components"
import "./ui/pages"
import "./ui/dialogs"

ApplicationWindow {
    id: window
    visible: true
    width: 360
    height: 660
    title: qsTr("Meu Relatório")
    font: ubuntu.name

    QtObject {
        id: object

        property color primary: Material.color(Material.Indigo)
        property color accent: Material.color(Material.Indigo, Material.Shade400)
        property date date: new Date()
        property int pageActive: 0
        property string titlePageActive: ""

        property color border_up: settings.theme === Material.Dark? Qt.darker(Material.foreground, 2) : Qt.darker(Material.background, 1.1)
        property color border_down: settings.theme === Material.Dark? Qt.darker(Material.background, 4.8) : Qt.lighter(Material.background, 1.1)
        property color background_item: settings.theme === Material.Dark? Qt.darker(Material.background, 6) : Qt.lighter(Material.background, 0.9)
        property color text_item: settings.theme === Material.Dark? Qt.darker(Material.background, 2.8) : Qt.lighter(Material.background, 0.6)

        property int swipeIndexReport: 0

        property string notification: ""
        onNotificationChanged: {
            if (notification !== "") {
                message._text = notification
                message.visible = true
            }
        }

        property var publicationsType
    }

    Settings {
        id: settings

        property int theme: Material.Light
        onThemeChanged: loadTheme()
    }

    header: headerContent

    DrawerMenu {
        id: leftMenu
    }

    StackView {
        id: stackView
        anchors.fill: parent

        initialItem: MainPage {}

        onDepthChanged: {
            window.header = stackView.data[stackView.depth -1].toolBar
            if (depth <= 1)
                object.pageActive = 0
        }
    }

    ToolTip {
        id: message
        property string _text

        timeout: 5000
        topMargin: parent.height -tooltip_text.parent.height -50
        x: (window.width - tooltip_text.parent.width) /2 -20
        z: 100

        width: 0
        height: 0

        Rectangle {
            width: tooltip_text.contentWidth + 10
            height: tooltip_text.contentHeight + 10

            color: Qt.rgba(Material.background.r, Material.background.g, Material.background.b, 0.8)
            radius: 3

            Column {
                anchors.fill: parent
                padding: 5

                Text {
                    id: tooltip_text
                    width: window.width *0.8
                    height: contentHeight
                    anchors.margins: 5

                    text: message._text
                    color: Material.foreground
                    wrapMode: Text.WordWrap
                }
            }
        }

        onVisibleChanged: if (!message.visible) object.notification = ""
    }

    ListModel {
        id: listPublicationsType

        property var rows

        function getRows() {
            rows = DB.get("PublicationsType")
        }

        function setRows() {
            clear()
            for (var it = 0; it < rows.length; ++it)
                append({
                   id: rows.item(it).id,
                   name: rows.item(it).name,
                   type: rows.item(it).type
               })
        }

        function filter(str, field) {
            clear()
            for (var it = 0; it < rows.length; ++it)
                if (rows.item(it)[field].match(new RegExp(str, "ig")))
                    append({
                       id: rows.item(it).id,
                       name: rows.item(it).name,
                       type: rows.item(it).type
                    })
        }

        onRowsChanged: setRows()
    }

    ListModel {
        id: listPublications

        property var rows

        function getRows() {
            rows = DB.getJoin(
                "Publications",
                ['Publications.*', 'PublicationsType.name as type'],
                ['PublicationsType'],
                ['Publications.pubTypeId=PublicationsType.id'],
                null,
                "PublicationsType.id ASC, name DESC"
            )
        }

        function setRows() {
            clear()
            for (var it = 0; it < rows.length; ++it)
                append({
                   id: rows.item(it).id,
                   name: rows.item(it).name,
                   file: rows.item(it).file === null? "" : rows.item(it).file,
                   pubTypeId: rows.item(it).pubTypeId,
                   type: rows.item(it).type === null? "Desconhecido" : rows.item(it).type,
                   link: rows.item(it).link === null? "" : rows.item(it).link
               })
        }

        function filter(str, field) {
            clear()
            for (var it = 0; it < rows.length; ++it)
                if (rows.item(it)[field].match(new RegExp(str, "ig")))
                    append({
                        id: rows.item(it).id,
                        name: rows.item(it).name,
                        file: rows.item(it).file === null? "" : rows.item(it).file,
                        pubTypeId: rows.item(it).pubTypeId,
                        type: rows.item(it).type === null? "Desconhecido" : rows.item(it).type,
                        link: rows.item(it).link === null? "" : rows.item(it).link
                    })
        }

        onRowsChanged: setRows()
    }

    ListModel {
        id: listMediaType

        property var rows

        function getRows() {
            rows = DB.get("MediaType")
        }

        function setRows() {
            clear()
            for (var it = 0; it < rows.length; ++it)
                append({
                   id: rows.item(it).id,
                   name: rows.item(it).name,
                   type: rows.item(it).type
               })
        }

        function filter(str, field) {
            clear()
            for (var it = 0; it < rows.length; ++it)
                if (rows.item(it)[field].match(new RegExp(str, "ig")))
                    append({
                       id: rows.item(it).id,
                       name: rows.item(it).name,
                       type: rows.item(it).type
                    })
        }

        onRowsChanged: setRows()
    }

    ListModel {
        id: listMedia

        property var rows: []

        function getRows() {
            rows = DB.getJoin(
                        "Media",
                        ['Media.*', 'MediaType.name as type'],
                        ['MediaType'],
                        ['Media.mediaTypeId = MediaType.id'],
                        null,
                        'MediaType.id ASC, name DESC'
                        )
        }

        function setRows() {
            clear()
            for (var it = 0; it < rows.length; ++it)
                append({
                           id: rows.item(it).id,
                           name: rows.item(it).name,
                           code: rows.item(it).code,
                           file: rows.item(it).file === null? "" : rows.item(it).file,
                           mediaTypeId: rows.item(it).mediaTypeId,
                           type: rows.item(it).type === null? "Desconhecido" : rows.item(it).type,
                           link: rows.item(it).link === null? "" : rows.item(it).link,
                           time: rows.item(it).time
                       })
        }

        function filter(str, field) {
            clear()
            for (var it = 0; it < rows.length; ++it)
                if (rows.item(it)[field].match(new RegExp(str, "ig")))
                    append({
                        id: rows.item(it).id,
                        name: rows.item(it).name,
                        code: rows.item(it).code,
                        file: rows.item(it).file === null? "" : rows.item(it).file,
                        mediaTypeId: rows.item(it).mediaTypeId,
                        type: rows.item(it).type === null? "Desconhecido" : rows.item(it).type,
                        link: rows.item(it).link === null? "" : rows.item(it).link,
                        time: rows.item(it).time
                    })
        }

        onRowsChanged: setRows()
    }

    ListModel {
        id: listEntry
    }

    Loader {
        id: loadDialog
        active: false
    }

    function openPage(page) {
        stackView.push(page)
    }

    function openDialog(m_dialog, parameters) {
        if (parameters === undefined)
            parameters = {visible: true}
        else
            parameters.visible = true
        loadDialog.setSource("qrc:/ui/dialogs/" + m_dialog + ".qml", parameters)
        loadDialog.active = true
    }

    function closeDialog() {
        loadDialog.setSource("")
        loadDialog.active = false
    }

    function loadTheme() {
        Material.theme = settings.theme
        Material.accent = object.accent
        Material.primary = object.primary

        if (Material.theme === Material.Dark) {
            Material.background = "#222222"
        } else {
            Material.background = "#FAFAFA"
        }
    }

    function reconfigure() {
        var publications = DB.get("Publications", "file != ''")
        for (var i = 0; i < publications.length; ++i)
            downloadManager.removeFile(publications.item(i).file)

        stackView.pop()
        DB.destroy()
        settings.theme = Material.Light

        DB.init()

        listPublicationsType.getRows()
        listPublications.getRows()

        listMediaType.getRows()
        listMedia.getRows()
    }

    Component.onCompleted: {
        loadTheme()
        DB.init()

        listPublicationsType.getRows()
        listPublications.getRows()
        listMediaType.getRows()
        listMedia.getRows()
    }

    DownloadManager {
        id: downloadManager
    }

    FontLoader {
        id: ubuntu
        source: "qrc:/fonts/Ubuntu.ttf"
    }

    FontLoader {
        id: fa
        source: "qrc:/fonts/fa-solid-900.ttf"
    }

    Share {
        id: share
    }

    StatusBar {
        id: statusBar
        color: object.primary
    }
}
