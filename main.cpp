#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QObject>
#include <QtQml/QQmlContext>

#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QString>
#include <QDebug>

#include <src/utils/statusbar/statusbar.h>
#include <src/utils/share/share.h>
#include <src/utils/download/downloadmanager.h>

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication::setApplicationName("Meu Relatório");
    QGuiApplication::setOrganizationName("Marssola");
    QCoreApplication::setApplicationVersion("1.0");
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication::setAttribute(Qt::AA_UseHighDpiPixmaps);
    QGuiApplication app(argc, argv);

    qmlRegisterType<Share>("com.Utils.Share", 0, 1, "Share");
    qmlRegisterType<StatusBar>("com.Utils.StatusBar", 0, 1, "StatusBar");
    qmlRegisterType<DownloadManager>("com.Utils.DownloadManager", 0, 1, "DownloadManager");

    QQmlApplicationEngine engine;
    QQmlContext *context = engine.rootContext();
    context->setContextProperty("PATH", QCoreApplication::applicationDirPath());

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
