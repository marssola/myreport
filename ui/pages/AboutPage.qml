import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.4

import "../navigation"

Flickable {
    contentHeight: pane.implicitHeight

    property ToolBar toolBar: HeaderPages {
        title: qsTr("Sobre")
    }

    Pane {
        id: pane

        width: parent.width
        height: parent.height

        Label {
            text: qsTr("Sobre")
        }
    }
}
