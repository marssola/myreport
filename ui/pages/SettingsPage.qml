import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.4

import "../navigation"
import "../Components"

Flickable {    
    contentHeight: pane.implicitHeight

    property ToolBar toolBar: HeaderPages {
        title: qsTr("Configurações")
    }

    Pane {
        id: pane
        property int margin: 20

        width: parent.width
        height: parent.height
        padding: 0


        Column {
            width: parent.width

            Section {
                text: qsTr("Aparência")
            }

            SwitchDelegate {
                width: parent.width
                text: qsTr("Tema Dark")

                checked: settings.theme
                onCheckedChanged: settings.theme = checked
            }

            Item {
                width: parent.width
                height: pane.margin
            }

            ItemDelegate {
                width: parent.width
                text: qsTr("Reconfigurar o App")

                onClicked: popupReconfigure.open()

                Popup {
                    id: popupReconfigure

                    width: (parent.width * 0.8) > 300? 300 : parent.width * 0.8
                    padding: 5
                    x: (parent.width - width) / 2
                    y:  -50

                    Column {
                        width: parent.width

                        RowLayout {
                            width: parent.width

                            Label {
                                text: "\uf57a"
                                font.family: fa.name
                                font.pixelSize: 30
                                color: Material.color(Material.Red, Material.Shade600)
                            }

                            Label {
                                Layout.fillWidth: true
                                Layout.margins: 5
                                wrapMode: Label.WordWrap
                                text: qsTr("Tem certeza que deseja reconfigurar o Aplicativo?")
                                font.pixelSize: 18
                                color: Material.color(Material.Red, Material.Shade600)
                            }
                        }

                        RowLayout {
                            width: parent.width

                            Button {
                                Layout.fillWidth: true
                                text: qsTr("Sim")
                                Material.background: Material.color(Material.Red, Material.Shade600)
                                Material.foreground: "#FFF"

                                onClicked:  {
                                    window.reconfigure()
                                    popupReconfigure.close()
                                }
                            }

                            Button {
                                Layout.fillWidth: true
                                flat: true
                                text: qsTr("Não")

                                onClicked: popupReconfigure.close()
                            }
                        }
                    }
                }
            }
        }
    }
}

