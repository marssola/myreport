import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.4
import QtQuick.LocalStorage 2.0

import "../functions/Database.js" as DB
import "../navigation"
import "../Components"
import "../dialogs"

Flickable {
    property int scrollOrigin: 0
    property bool buttonBottom: true
    property bool filter: false

    property ToolBar toolBar: ToolBar {
        Material.background: object.primary

        ColumnLayout {
            width: parent.width

            RowLayout {
                Layout.fillWidth: true

                ToolButton {
                    text: "\uf060"
                    font.family: fa.name
                    font.pixelSize: 20
                    Material.foreground: "#FFF"

                    onClicked: stackView.pop()
                }

                Label {
                    Layout.fillWidth: true
                    text: qsTr("Publicações")
                    Material.foreground: "#FFF"
                    font.pixelSize: 20
                    elide:  Label.ElideRight
                    verticalAlignment: Label.AlignBottom
                }

                ToolButton {
                    text: "\uf381"
                    font.family: fa.name
                    Material.foreground: "#FFF"

                    onClicked: {
                        openDialog("DialogPublicationsSync")
                    }
                }
            }

            SearchField {
                id: searchField

                Layout.fillWidth: true
                Layout.leftMargin: 8
                Layout.rightMargin: 8
                Layout.bottomMargin: 10

                backgroundColor: Qt.lighter(object.primary, 0.9)
                textColor: "#fff"

                visible: listPublications.count
                placeholderText: qsTr("Procurar publicação")

                onTextChanged: typing()
                onEditingFinished: typing()
                Keys.onReleased: typing()

                function typing() {
                    filter = length
                    listPublications.filter(text, 'name')
                }
            }
        }
    }

    Pane {
        id: panePublications
        anchors.fill: parent
        padding: 2

        ColumnLayout {
            anchors.fill: parent

            ColumnLayout {
                visible: !listPublications.count
                Layout.fillHeight: true
                Layout.fillWidth: true

                Label {
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    text: "\uf5b4"
                    font.family: fa.name
                    font.pixelSize: 30
                    opacity: 0.5

                    verticalAlignment: Label.AlignBottom
                    horizontalAlignment: Label.AlignHCenter
                }
                Label {
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    text: qsTr("Não há nada por aqui")
                    horizontalAlignment: Label.AlignHCenter
                    opacity: 0.5
                }
            }

            ListView {
                id: listViewPublications
                Layout.fillHeight: true
                Layout.fillWidth: true

                visible: !filter
                enabled: visible
                clip: true

                property string sectionVisibility: ""

                onMovementStarted: {
                    scrollOrigin = contentY
                    buttonBottom = (contentY < scrollOrigin)
                }
                onMovementEnded: {
                    if (contentY > scrollOrigin)
                        scrollOrigin = contentY
                    else
                        buttonBottom = true
                    if (!scrollOrigin)
                        buttonBottom = true
                }

                model: listPublications
                delegate: SwipePublication {
                    visible: listViewPublications.sectionVisibility === ListView.section
                    height: visible? 70: 0
                }

                section.property: "type"
                section.criteria: ViewSection.FullString
                section.delegate: ItemDelegate {
                    id: sectionDelegate
                    visible: !filter
                    enabled: !filter
                    width: parent.width
                    height: filter? 0 : 48
                    opacity: filter? 0 : 0.5

                    highlighted: listViewPublications.sectionVisibility === section

                    contentItem: RowLayout {
                        anchors.fill: parent

                        Label {
                            Layout.fillHeight: true
                            Layout.fillWidth: true
                            Layout.margins: 5
                            verticalAlignment: Label.AlignVCenter

                            text: section
                            font.bold: true
                            font.capitalization: Font.AllUppercase
                            elide: Label.ElideRight
                        }

                        Label {
                            Layout.fillHeight: true
                            Layout.preferredWidth: 30
                            verticalAlignment: Label.AlignVCenter
                            horizontalAlignment: Label.AlignHCenter

                            text: "\uf107"
                            font.family: fa.name
                            rotation: listViewPublications.sectionVisibility === section? 180 : 0

                            Behavior on rotation {
                                NumberAnimation {
                                    duration: 150
                                }
                            }
                        }
                    }

                    Rectangle {
                        width: parent.width
                        height: 1
                        anchors.bottom: parent.bottom
                        anchors.bottomMargin: 1
                        color: object.border_up
                    }

                    Rectangle {
                        width: parent.width
                        height: 1
                        anchors.bottom: parent.bottom
                        color: object.border_down
                    }

                    onClicked: listViewPublications.sectionVisibility = (listViewPublications.sectionVisibility === section)? "" : section
                }
            }

            ListView {
                id: listViewFilteredPublications
                Layout.fillHeight: true
                Layout.fillWidth: true

                visible: filter
                enabled: visible
                clip: true

                model: listPublications
                delegate: SwipePublication {
                    height: 70
                }
            }
        }

        RoundButton {
            visible: buttonBottom && !filter
            width: 70
            height: width
            text: "\uf067"
            font.family: fa.name
            font.pixelSize: 20

            Material.foreground: "#FFF"
            Material.background: object.primary

            anchors.bottom: parent.bottom
            anchors.bottomMargin: buttonBottom? 15 : - (width + 10)
            anchors.right: parent.right
            anchors.rightMargin: 15

            Behavior on anchors.bottomMargin {
                NumberAnimation {
                    duration: 150
                }
            }

            onClicked: openDialog("DialogAddPublication")
        }
    }
}
