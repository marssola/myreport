import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.4

import "../navigation"
import "../Components"
import "../navigation"

SwipeView {
    id: swipeReport

    currentIndex: object.swipeIndexReport
    onCurrentIndexChanged: object.swipeIndexReport = currentIndex

    property ToolBar toolBar: MainToolBar {}

    Flickable {
        contentHeight: paneReport.implicitHeight

        Pane {
            id: paneReport

            width: parent.width
            height: parent.height
            padding: 0

            Column {
                width: parent.width

                Item {
                    width: parent.width
                    height: 15
                }

                ItemReport {
                    icon: "\uf02d"
                    text: qsTr("Publicações")
                    value: 0
                }

                ItemReport {
                    icon: "\uf008"
                    text: qsTr("Vídeos")
                    value: 0
                }

                ItemReport {
                    icon: "\uf017"
                    text: qsTr("Horas")
                    value: 0
                }

                ItemReport {
                    icon: "\uf0c0"
                    text: qsTr("Revisitas")
                    value: 0
                }

                ItemReport {
                    icon: "\uf5da"
                    text: qsTr("Estudos")
                    value: 0
                    borderBottom: false
                }

                Item {
                    width: parent.width
                    height: 15
                }
            }

            RoundButton {
                width: 70
                height: width
                text: "\uf067"
                font.family: fa.name
                font.pixelSize: 20

                Material.foreground: "#FFF"
                Material.background: object.primary

                parent: swipeReport
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 20
                anchors.right: parent.right
                anchors.rightMargin: 20
            }
        }
    }

    Pane {
        id: paneEntries
        padding: 0

        ColumnLayout {
            anchors.fill: parent

            Label {
                Layout.fillWidth: true
                Layout.fillHeight: true
                text: "\uf5b4"
                font.family: fa.name
                font.pixelSize: 30
                opacity: 0.5

                verticalAlignment: Label.AlignBottom
                horizontalAlignment: Label.AlignHCenter
            }

            Label {
                visible: !listEntry.count
                Layout.fillHeight: true
                Layout.fillWidth: true
                text: qsTr("Nenhuma entrada inserida")
                horizontalAlignment: Label.AlignHCenter
                opacity: 0.5
            }
        }
    }
}
