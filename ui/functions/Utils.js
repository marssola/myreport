function getMonth(month, substr)
{
    var m_month = ""
    switch (month) {
        case 0: m_month = qsTr("Janeiro"); break
        case 1: m_month = qsTr("Fevereiro"); break
        case 2: m_month = qsTr("Março"); break
        case 3: m_month = qsTr("Abril"); break
        case 4: m_month = qsTr("Maio"); break
        case 5: m_month = qsTr("Junho"); break
        case 6: m_month = qsTr("Julho"); break
        case 7: m_month = qsTr("Agosto"); break
        case 8: m_month = qsTr("Setembro"); break
        case 9: m_month = qsTr("Outubro"); break
        case 10: m_month = qsTr("Novembro"); break
        case 11: m_month = qsTr("Dezembro"); break
    }
    if (substr !== undefined)
        return m_month.substring(0, 3)
    return m_month;
}
