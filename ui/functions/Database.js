var db = LocalStorage.openDatabaseSync("MyReport", "1.0", "Database", 1000000);

function init()
{
    try {
        db.transaction(function (tx) {
            tx.executeSql("CREATE TABLE IF NOT EXISTS PublicationsType(id INTEGER PRIMARY KEY AUTOINCREMENT, name VARCHAR, type VARCHAR, standard BOOLEAN)")
            tx.executeSql("CREATE TABLE IF NOT EXISTS Publications(id INTEGER PRIMARY KEY AUTOINCREMENT, code VARCHAR, name VARCHAR, file VARCHAR, pubTypeId INT, link VARCHAR)")
            tx.executeSql("CREATE TABLE IF NOT EXISTS MediaType(id INTEGER PRIMARY KEY AUTOINCREMENT, name VARCHAR, type VARCHAR)")
            tx.executeSql("CREATE TABLE IF NOT EXISTS Media(id INTEGER PRIMARY KEY AUTOINCREMENT, code VARCHAR, name, file VARCHAR, mediaTypeId INT, link VARCHAR, time VARCHAR)")
        });
    } catch (err) {
        console.log("Error trying to create tables: " + err)
    }
}

function destroy()
{
    try {
        db.transaction(function (tx) {
            tx.executeSql("DROP TABLE IF EXISTS PublicationsType")
            tx.executeSql("DROP TABLE IF EXISTS Publications")
            tx.executeSql("DROP TABLE IF EXISTS MediaType")
            tx.executeSql("DROP TABLE IF EXISTS Media")
        });
    } catch (err) {
        console.log("Error trying to drop table: " + err)
    }
}

// ----------------------------------------------------------------------------------------------------------------------------------------- CRUD

function add(table, data)
{
    if (!table) {
        console.log("Error: table not defined")
        return
    }

    if (!data) {
        console.log("Error: data not defined")
        return
    }

    var insert_data = {
        keys: [],
        values: []
    }
    for (var k in data) {
        insert_data.keys.push(k)
        insert_data.values.push((typeof data[k] === 'number'? data[k] : "'" + data[k] + "'"))
    }

    db.transaction(function (tx) {
        try {
            tx.executeSql("INSERT INTO " + table + " (" + insert_data.keys.join(', ') + ") VALUES(" + insert_data.values.join(', ') + ")")
        } catch (err) {
            console.log("Error trying to insert: " + err)
        }
    })
}

function getOne(table, where)
{
    if (!where)
        where = "id > 0"
    var row = []
    try {
        db.transaction(function (tx) {
            var get = tx.executeSql("SELECT * FROM " + table + " WHERE " + where)
            if (get.rows.length)
                row = get.rows.item(0)
        })
    } catch (err) {
        console.log("Error trying to get one record: " + err)
    }
    return row;
}

function get(table, where)
{
    if (!where)
        where = "id > 0"
    var content = []
    try {
        db.transaction(function (tx) {
            var get = tx.executeSql("SELECT * FROM " + table + " WHERE " + where)
            if (get.rows.length)
                content = get.rows
        })
    } catch (err) {
        console.log("Error trying to get: " + err)
    }
    return content
}

function getJoin(table, fields, joins, reference, where, order)
{
    var content = []
    try {
        db.transaction(function (tx) {
            var query = "SELECT " + fields.join(', ') + " FROM " + table + " "
            for (var j in joins) {
                query += "LEFT JOIN " + joins[j] + " ON " + reference[j]
            }
            if (where)
                query += " WHERE " + where
            if (order)
                query += " ORDER BY " + order
            var get = tx.executeSql(query)
            if (get.rows.length)
                content = get.rows
        })
    } catch (err) {
        console.log("Error trying to get with join: " + err)
    }
    return content
}

function update(table, where, data)
{
    var data_update = [], query;
    for (var key in data)
        data_update.push(key + "=" + (typeof data[key] === 'number' ? data[key] : "'" + data[key] + "'") + "");
    query = "UPDATE " + table + " SET " + data_update.join(', ') + " WHERE " + where;

    db.transaction(function (tx) {
        try {
            tx.executeSql(query);
            result = true;
        } catch (err) {
            console.log("Error trying to update: " + err);
        }
    });
}

function remove(table, where)
{
    db.transaction(function (tx) {
        try {
            tx.executeSql("DELETE FROM " + table + " WHERE " + where);
        } catch (err) {
            console.log("Error trying to delete: " + err);
        }
    });
}

// ----------------------------------------------------------------------------------------------------------------------------------------- CRUD END

function completeZero(str, length)
{
    if (length === undefined)
        length = 2;
    str = str.toString();
    while (str.length < length) {
        str = "0" + str;
    }
    return str;
}
