import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Controls.Material 2.2

Label {
    width: parent.width
    font.capitalization: Font.AllUppercase
    color: Qt.rgba(Material.foreground.r, Material.foreground.g, Material.foreground.b, 0.5)
    padding: 10
}
