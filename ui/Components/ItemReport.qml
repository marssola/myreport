import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Controls.Material 2.2
import QtQuick.Layouts 1.3

Item {
    id: control
    width: parent.width
    height: 60

    property string icon
    property string iconSource: fa.name
    property string text
    property int value: 0
    property bool borderBottom: true

    RowLayout {
         anchors.fill: parent
         anchors.margins: 10

         Label {
             text: control.icon
             font.family: control.iconSource
             font.pixelSize: 25
             Layout.leftMargin: 10
             Layout.rightMargin: 10
         }

         Label {
             text: control.text
             Layout.fillWidth: true
             font.capitalization: Font.AllUppercase
         }

         Label {
             text: control.value
             font.pixelSize: 20
             Layout.leftMargin: 10
             Layout.rightMargin: 10
         }
    }

    Rectangle {
        visible: control.borderBottom
        width: parent.width
        height: 1
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 1
        color: object.border_up
    }

    Rectangle {
        visible: control.borderBottom
        width: parent.width
        height: 1
        anchors.bottom: parent.bottom
        color: object.border_down
    }
}
