import QtQuick 2.11
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.4
import QtQuick.Controls.impl 2.4
import QtQuick.Controls.Material 2.4
import QtQuick.Controls.Material.impl 2.4

Dialog {
    id: control

    background: Rectangle {
        radius: 2
        color: control.Material.dialogColor
        layer.enabled: control.Material.elevation > 0
        layer.effect: ElevationEffect {
            elevation: control.Material.elevation
        }
    }

    header: Label {
        text: control.title
        visible: control.title
        elide: Label.ElideRight

        padding: 20
        bottomPadding: -20
        font.bold: true
        font.pixelSize: 16
        background: Rectangle {
            radius: 2
            color: control.Material.dialogColor
        }
    }

//    Component.onCompleted: {
//        padding = 5
//    }
}
