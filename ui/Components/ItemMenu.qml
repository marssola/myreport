import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

ItemDelegate {
    id: control
    property var iconFont
    property string iconSourceFont: fa.name

    Layout.fillWidth: true
    contentItem: RowLayout {
        anchors.fill: parent
        spacing: 10

        Label {
            Layout.preferredWidth: 20
            Layout.fillHeight: true
            Layout.leftMargin: 10
            Layout.rightMargin: 10

            text: control.iconFont
            font.family: control.iconSourceFont
            font.pixelSize: 18

            horizontalAlignment: Label.AlignHCenter
            verticalAlignment: Label.AlignVCenter
        }

        Label {
            Layout.fillWidth: true
            Layout.fillHeight: true

            verticalAlignment: Label.AlignVCenter

            text: control.text
        }
    }
}
