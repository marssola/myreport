import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.4
import QtQuick.LocalStorage 2.0

import "../functions/Database.js" as DB

SwipeDelegate {
    id: swipePublications

    width: parent.width
    leftPadding: 0
    rightPadding: 0
    topPadding: 0
    bottomPadding: 0
    padding: 2

    contentItem: RowLayout {
        width: parent.width
        height: parent.height

        Item {
            Layout.fillHeight: true
            Layout.preferredWidth: height

            Label {
                visible: imgPub.status === Image.Null || imgPub.status === Image.Error
                anchors.fill: parent
                verticalAlignment: Label.AlignVCenter
                horizontalAlignment: Label.AlignHCenter

                text: "\uf03e"
                color: Material.foreground
                opacity: 0.5
                font.family: fa.name
                font.pixelSize: height * 0.5
            }

            Image {
                id: imgPub
                visible: Image.Ready

                source: file !== ""? "file://" + file : ""
                anchors.fill: parent
                anchors.centerIn: parent
                fillMode: Image.PreserveAspectCrop
                clip: true
            }
        }

        Label {
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.margins: 2

            text: name
            wrapMode: Label.WordWrap
            elide: Label.ElideRight
            verticalAlignment: Label.AlignVCenter
        }
    }

    Rectangle {
        width: parent.width
        height: 1
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 1
        color: object.border_up
    }

    Rectangle {
        width: parent.width
        height: 1
        anchors.bottom: parent.bottom
        color: object.border_down
    }

    swipe.right: Label {
        id: deleteLabel
        text: "\uf1f8"
        font.family: fa.name
        font.pixelSize: 18
        color: Material.color(Material.Red)

        anchors.right: parent.right
        height: parent.height
        width: 50
        verticalAlignment: Label.AlignVCenter
        horizontalAlignment: Label.AlignHCenter

        background: Rectangle  {
            color: mouseareaSwipeDeletePublications.pressed? Qt.darker(Material.background) : Qt.lighter(Material.background)

            MouseArea {
                id: mouseareaSwipeDeletePublications
                anchors.fill: parent

                onClicked: {
                    deleteConfirm.open()
                }
            }

            Popup {
                id: deleteConfirm
                width: 250
                padding: 5

                x: -width
                y: -height * 0.5

                Column {
                    width: parent.width

                    RowLayout {
                        width: parent.width

                        Label {
                            text: "\uf57a"
                            font.family: fa.name
                            font.pixelSize: 30
                            color: Material.color(Material.Red, Material.Shade600)
                        }

                        Label {
                            Layout.fillWidth: true
                            wrapMode: Label.WordWrap
                            padding: 10
                            text: qsTr("Tem certeza que deseja remover esta publicação?")
                        }
                    }

                    RowLayout {
                        width: parent.width

                        Button {
                            Layout.fillWidth: true
                            text: qsTr("Sim")
                            Material.background: Material.color(Material.Red, Material.Shade600)
                            Material.foreground: "#FFF"

                            onClicked:  {
                                var publication = DB.getOne("Publications", "id=" + id)
                                downloadManager.removeFile(publication.file)
                                DB.remove('Publications', "id=" + id)
                                listPublications.getRows()
                                deleteConfirm.close()
                            }
                        }

                        Button {
                            Layout.fillWidth: true
                            flat: true
                            text: qsTr("Não")

                            onClicked: {
                                swipePublications.swipe.close()
                                deleteConfirm.close()
                            }
                        }
                    }
                }
            }
        }
    }

    swipe.left: RowLayout {
        id: rowFunctions

        width: 100
        height: parent.height
        spacing: 0

        Label {
            id: shareLabel
            enabled: link !== ""

            text: link !== ""? "\uf1e0" : "\uf5c2"
            font.family: fa.name
            font.pixelSize: 18

            Layout.fillHeight: true
            Layout.preferredWidth: 25
            Layout.fillWidth: true

            verticalAlignment: Label.AlignVCenter
            horizontalAlignment: Label.AlignHCenter

            background: Rectangle {
                color: mouseAreaSwipeSharePublication.pressed? Qt.darker(Material.background) : Qt.lighter(Material.background)
                opacity: 0.5

                MouseArea {
                    id: mouseAreaSwipeSharePublication
                    anchors.fill: parent

                    onClicked: {
                        share.share(link)
                        swipe.close()
                    }
                }
            }
        }

        Label {
            id: openLabel
            visible: link !== ""

            text: "\uf35d"
            font.family: fa.name
            font.pixelSize: 18

            Layout.fillHeight: true
            Layout.preferredWidth: 25
            Layout.fillWidth: true

            verticalAlignment: Label.AlignVCenter
            horizontalAlignment: Label.AlignHCenter

            background: Rectangle {
                color: mouseAreaSwipeOpenPublication.pressed? Qt.darker(Material.background) : Qt.lighter(Material.background)
                opacity: 0.5

                MouseArea {
                    id: mouseAreaSwipeOpenPublication
                    anchors.fill: parent

                    onClicked: {
                        Qt.openUrlExternally(link)
                        swipe.close()
                    }
                }
            }
        }
    }

    onClicked: {
        swipe.close()
        openDialog("DialogAddPublication", {publicationId: id})
    }
}
