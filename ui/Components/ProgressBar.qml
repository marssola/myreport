import QtQuick.Templates 2.4 as T
import QtQuick 2.11
import QtQuick.Controls.Material 2.4
import QtQuick.Controls.Material.impl 2.4

T.ProgressBar {
    id: control

    implicitWidth: Math.max(background ? background.implicitWidth : 0,
                            contentItem.implicitWidth + leftPadding + rightPadding)
    implicitHeight: Math.max(background ? background.implicitHeight : 0,
                             contentItem.implicitHeight + topPadding + bottomPadding)

    contentItem: ProgressBarImpl {
        implicitHeight: 1

        scale: control.mirrored ? -1 : 1
        color: control.Material.accentColor
        progress: control.position
        indeterminate: control.visible && control.indeterminate
    }

    background: Rectangle {
        implicitWidth: 200
        implicitHeight: 1
        y: (control.height - height) / 2
        height: 1

        color: Qt.rgba(control.Material.accentColor.r, control.Material.accentColor.g, control.Material.accentColor.b, 0.25)
    }
}
