import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.4
import QtQuick.Controls.impl 2.2

import "./"

TextField {
    id: control

    property color backgroundColor: object.background_item
    property color placeholderColor: Qt.rgba(textColor.r, textColor.g, textColor.b, 0.5)
    property color textColor: object.text_item

    padding: 5
    topPadding: 8
    bottomPadding: 8
    leftPadding: 30
    rightPadding: 30

    selectedTextColor: color
    verticalAlignment: TextInput.AlignVCenter
    selectByMouse: true

    color: textColor

    PlaceholderText {
        id: placeholder
        x: control.leftPadding
        y: control.topPadding
        width: control.width - (control.leftPadding + control.rightPadding)
        height: control.height - (control.topPadding + control.bottomPadding)

        text: control.placeholderText
        font: control.font
        color: control.placeholderColor
        verticalAlignment: control.verticalAlignment
        visible: !control.length && !control.preeditText && (!control.activeFocus || control.horizontalAlignment !== Qt.AlignHCenter)
        elide: Text.ElideRight
    }

    background: Item {
        Rectangle {
            anchors.fill: parent
            radius: 50
            color: control.backgroundColor
        }

        Label {
            x: 10
            y: (parent.height /2) - (height /2)

            text: "\uf002"
            font.family: fa.name
            color: control.placeholderColor
        }

        Label {
            visible: control.length || control.activeFocus
            width: 30
            height: parent.height
            x: parent.width - width
            y: (parent.height /2) - (height /2)

            text: "\uf057"
            font.family: fa.name
            color: control.placeholderColor

            verticalAlignment: Label.AlignVCenter
            horizontalAlignment: Label.AlignHCenter

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    searchField.clear()
                    typing()
                }
            }
        }
    }
}
