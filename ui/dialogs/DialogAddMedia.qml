import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.4
import QtQuick.LocalStorage 2.0

import "../functions/Database.js" as DB
import "../Components"

Dialog {
    id: dialog

    title: qsTr("Adicionar Media")

    property int mediaId
    onMediaIdChanged: {
        if (mediaId < 1)
            return

        var media = DB.get("Media", "id=" + mediaId)
        if (media.length) {
            mediaName.text = media.item(0).name
            for (var k = 0; k < listMediaType.count; ++k)
                if (media.item(0).mediaTypeId === listMediaType.get(k).id)
                    mediaType.currentIndex = k
        } else {
            mediaId = undefined
        }
    }

    width: window.width < 400? window.width - 10 : 400
    height: window.height < 280? window.height : 280
    parent: ApplicationWindow.overlay
    x: (window.width - width) / 2
    y: (window.height - height) / 2

    modal: true
    closePolicy: Popup.NoAutoClose

    Flickable {
        anchors.fill: parent
        contentHeight: column.implicitHeight
        clip: true

        Column {
            id: column
            width: parent.width

            Item {
                width: parent.width
                height: 15
            }

            Label {
                width: parent.width
                text: qsTr("Nome")
                font.bold: true
            }
            TextField {
                id: mediaName
                width: parent.width

                Keys.onEnterPressed: buttonSave.clicked()
                Keys.onReturnPressed: buttonSave.clicked()
            }

            Item {
                width: parent.width
                height: 15
            }

            Label {
                width: parent.width
                text: qsTr("Tipo de Vídeo")
                font.bold: true
            }
            ComboBox {
                id: mediaType
                width: parent.width
                currentIndex: -1

                textRole: "name"
                model: listMediaType
            }

            Item {
                width: parent.width
                height: 15
            }
        }
    }

    Component.onCompleted: padding = 10
    onClosed: closeDialog()

    footer: DialogButtonBox {
        Button {
            text: qsTr("Cancel")
            flat: true

            onClicked: {
                dialog.close()
            }
        }
        Button {
            id: buttonSave
            text: qsTr("Save")
            flat: true

            onClicked: {
                if (mediaName.text === "") {
                    object.notification = qsTr("*É obrigatório definir o nome da publicação")
                    return
                }

                if (mediaType.currentText === "") {
                    object.notification = qsTr("*É obrigatório definir o tipo da publicação")
                    return
                }

                if (dialog.mediaId)
                    DB.update('Media', 'id=' + dialog.mediaId, {name: mediaName.text, mediaTypeId: mediaType.model.get(mediaType.currentIndex).id})
                else
                    DB.add('Media', {name: mediaName.text, mediaTypeId: mediaType.model.get(mediaType.currentIndex).id})
                listMedia.getRows()
                dialog.close()
            }
        }
    }
}
