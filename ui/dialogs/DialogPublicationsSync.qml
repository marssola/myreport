import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.4
import QtQuick.LocalStorage 2.0

import com.Utils.DownloadManager 0.1
import "../functions/Database.js" as DB
import "../Components"

Dialog {
    id: dialogDownloadManager

    title: qsTr("Atualizar Publicações")
    width: window.width < 400? window.width - 10 : 400
    height: window.height < 600? window.height - 10 : 600
    x: (window.width - width) / 2
    y: (window.height - height) / 2

    modal: true
    parent: ApplicationWindow.overlay
    standardButtons: Dialog.Close

    ColumnLayout {
        anchors.fill: parent

        ColumnLayout {
            visible: !listModelDownload.count
            Layout.fillHeight: true
            Layout.fillWidth: true

            Label {
                Layout.fillWidth: true
                Layout.fillHeight: true
                text: "\uf59c"
                font.family: fa.name
                font.pixelSize: 30
                opacity: 0.5

                verticalAlignment: Label.AlignBottom
                horizontalAlignment: Label.AlignHCenter
            }
            Label {
                Layout.fillHeight: true
                Layout.fillWidth: true
                text: qsTr("Não há nada para atualizar")
                horizontalAlignment: Label.AlignHCenter
                opacity: 0.5
            }
        }

        ListModel {
            id: listModelDownload
        }

        ListView {
            id: listViewDownload

            Layout.fillHeight: true
            Layout.fillWidth: true
            clip: true
            visible: listModelDownload.count

            model: listModelDownload
            delegate: ItemDelegate {
                width: parent.width
                height: 70

                contentItem: RowLayout {
                    anchors.fill: parent

                    Item {
                        Layout.preferredWidth: height
                        Layout.fillHeight: true

                        Layout.margins: 2

                        BusyIndicator {
                            anchors.fill: parent
                            padding: 25
                            running: imgPub.status === Image.Loading
                        }

                        Label {
                            visible: imgPub.status === Image.Null || imgPub.status === Image.Error
                            anchors.fill: parent
                            verticalAlignment: Label.AlignVCenter
                            horizontalAlignment: Label.AlignHCenter

                            text: "\uf03e"
                            color: Material.foreground
                            opacity: 0.5
                            font.family: fa.name
                            font.pixelSize: height * 0.5
                        }

                        Image {
                            id: imgPub
                            visible: Image.Ready

                            source: file === "" || !file? "" : "file://" + file
                            width: parent.width
                            height: parent.height
                            anchors.centerIn: parent
                            fillMode: Image.PreserveAspectCrop
                        }
                    }

                    Label {
                        Layout.fillWidth: true
                        padding: 5

                        text: name
                        wrapMode: Label.WordWrap
                    }

                    Label {
                        Layout.fillHeight: true
                        verticalAlignment: Label.AlignBottom
                        padding: 5

                        text: !downloaded? "\uf381" : "\uf058"
                        font.family: fa.name
                        color: Material.foreground
                        opacity: 0.6
                    }
                }

                ProgressBar {
                    id: progressBottom
                    visible: !downloaded
                    width: parent.width
                    anchors.bottom: parent.bottom
                    indeterminate: !value
                    value: typeof downloading === 'undefined'? 0.0 : downloading
                }

                Rectangle {
                    visible: downloaded
                    width: parent.width
                    height: 1
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 1
                    color: object.border_up
                }

                Rectangle {
                    visible: downloaded
                    width: parent.width
                    height: 1
                    anchors.bottom: parent.bottom
                    color: object.border_down
                }
            }
        }
    }

    DownloadManager {
        id: downloadManager

        property var publications: {
            "types": [
                {
                    "type": 'b',
                    "name": 'Livros',
                    'standard': true
                },
                {
                    "type": 't',
                    "name": 'Folhetos',
                    'standard': true
                },
                {
                    "type": 'bl',
                    "name": 'Brochuras e Livretos',
                    'standard': true
                },
                {
                    "type": 'inv',
                    "name": 'Convites',
                    'standard': true
                },
                {
                    "type": 'g',
                    "name": 'Despertai!',
                    'standard': true
                },
                {
                    "type": 'wp',
                    "name": 'A Sentinela Público',
                    'standard': true
                },
                {
                    "type": 'w',
                    "name": 'A Sentinela Estudo',
                    'standard': true
                }
            ],
            "publications": [
                {
                    "id": "g18-3",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/g/T/201811/wpub/g_T_201811_lg.jpg",
                    "name":"N.° 3 2018 | Como lidar com a dor da morte",
                    "type":"g",
                    "link": "https://www.jw.org/pt/publicacoes/revistas/despertai-no3-2018-nov-dez/"
                },
                {
                    "id": "g18-2",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/g/T/201807/wpub/g_T_201807_lg.jpg",
                    "name":"N.° 2 2018 | 12 Segredos para uma família ser feliz",
                    "type":"g",
                    "link": "https://www.jw.org/pt/publicacoes/revistas/despertai-no2-2018-jul-ago/"
                },
                {
                    "id": "g18-1",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/g/T/201803/wpub/g_T_201803_lg.jpg",
                    "name":"N.° 1 2018 | O caminho para a felicidade",
                    "type":"g",
                    "link": "https://www.jw.org/pt/publicacoes/revistas/despertai-n1-2018-mar-abr/"
                },
                {
                    "id": "wp18-3",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/wp/T/201809/wpub/wp_T_201809_lg.jpg",
                    "name":"N.° 3 2018 | Será que Deus se importa com você?",
                    "type":"wp",
                    "link": "https://www.jw.org/pt/publicacoes/revistas/sentinela-no3-2018-set-out/"
                },
                {
                    "id": "wp18-2",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/wp/T/201805/wpub/wp_T_201805_lg.jpg",
                    "name":"N.° 2 2018 | É possível saber o futuro?",
                    "type":"wp",
                    "link": "https://www.jw.org/pt/publicacoes/revistas/sentinela-no2-2018-maio-jun/"
                },
                {
                    "id": "wp18-1",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/wp/T/201801/wpub/wp_T_201801_lg.jpg",
                    "name":"N.° 1 2018 | Será que a Bíblia ainda é útil?",
                    "type":"wp",
                    "link": "https://www.jw.org/pt/publicacoes/revistas/sentinela-no1-2018-jan-fev/"
                },
                {
                    "id": "wp17-6",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/wp/T/201711/wpub/wp_T_201711_lg.jpg",
                    "name":"N.° 6 2017 | Um presente muito especial para você",
                    "type":"wp",
                    "link": "https://www.jw.org/pt/publicacoes/revistas/sentinela-no6-novembro-2017/"
                },
                {
                    "id": "g17-6",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/g/T/201712/wpub/g_T_201712_lg.jpg",
                    "name":"N.° 6 2017 | O mundo está fora de controle?",
                    "type": "g",
                    "link": "https://www.jw.org/pt/publicacoes/revistas/despertai-n6-dezembro-2017/"
                },
                {
                    "id": "g17-5",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/g/T/201710/wpub/g_T_201710_lg.jpg",
                    "name":"N.° 5 2017 | Tragédias — O que fazer quando sua vida está em jogo",
                    "type": "g",
                    "link": "https://www.jw.org/pt/publicacoes/revistas/despertai-n5-outubro-2017/"
                },
                {
                    "id": "g17-4",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/g/T/201708/wpub/g_T_201708_lg.jpg",
                    "name":"N.° 4 2017 | A sua vida é corrida demais?",
                    "type": "g",
                    "link": "N.° 4 2017 | A sua vida é corrida demais?"
                },
                {
                    "id": "g17-3",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/g/T/201706/wpub/g_T_201706_lg.jpg",
                    "name":"N.° 3 2017 | A Bíblia é mesmo a Palavra de Deus?",
                    "type": "g",
                    "link": "https://www.jw.org/pt/publicacoes/revistas/despertai-n3-junho-2017/"
                },
                {
                    "id": "g17-2",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/g/T/201704/wpub/g_T_201704_lg.jpg",
                    "name":"N.° 2 2017 | Os perigos por trás do sobrenatural",
                    "type": "g",
                    "link": "https://www.jw.org/pt/publicacoes/revistas/despertai-no2-abril-2017/"
                },
                {
                    "id": "g17-1",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/g/T/201702/wpub/g_T_201702_lg.jpg",
                    "name":"N.° 1 2017 | Depressão em adolescentes — Por quê? O que pode ajudar?",
                    "type": "g",
                    "link": "https://www.jw.org/pt/publicacoes/revistas/despertai-n1-2017-fevereiro/"
                },
                {
                    "id": "wp17-5",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/wp/T/201709/wpub/wp_T_201709_lg.jpg",
                    "name":"N.° 5 2017 | A verdade sobre os anjos",
                    "type": "wp",
                    "link": "https://www.jw.org/pt/publicacoes/revistas/sentinela-no5-setembro-2017/"
                },
                {
                    "id": "wp17-4",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/wp/T/201707/wpub/wp_T_201707_lg.jpg",
                    "name":"N.° 4 2017 | O que a Bíblia diz sobre vida e morte?",
                    "type": "wp",
                    "link": "https://www.jw.org/pt/publicacoes/revistas/sentinela-no4-julho-2017/"
                },
                {
                    "id": "wp17-3",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/wp/T/201705/wpub/wp_T_201705_lg.jpg",
                    "name":"N.° 3 2017 | Os cavaleiros do Apocalipse — Como afetam sua vida?",
                    "type": "wp",
                    "link": "https://www.jw.org/pt/publicacoes/revistas/wp201705/"
                },
                {
                    "id": "wp17-2",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/wp/T/201703/wpub/wp_T_201703_lg.jpg",
                    "name":"N.° 2 2017 | Você vai aceitar o melhor presente de Deus?",
                    "type": "wp",
                    "link": "https://www.jw.org/pt/publicacoes/revistas/wp201703/"
                },
                {
                    "id": "wp17-1",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/wp/T/201701/wpub/wp_T_201701_lg.jpg",
                    "name":"N.° 1 2017 | Como gostar de ler a Bíblia",
                    "type": "wp",
                    "link": "https://www.jw.org/pt/publicacoes/revistas/wp201701/"
                },
                {
                    "id": "bhs",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/bhs/T/wpub/bhs_T_lg.jpg",
                    "name":"Você Pode Entender a Bíblia!",
                    "type": "b",
                    "link": "https://www.jw.org/pt/publicacoes/livros/estudo-da-biblia/"
                },
                {
                    "id": "t-36",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/t-36/T/wpub/T-36_T_lg.jpg",
                    "name":"O que é o Reino de Deus?",
                    "type": "t",
                    "link": "https://www.jw.org/pt/publicacoes/livros/folheto-reino-de-deus/"
                },
                {
                    "id": "t-37",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/t-37/T/wpub/T-37_T_lg.jpg",
                    "name":"Onde Encontrar as Respostas mais Importantes da Vida?",
                    "type": "t",
                    "link": "https://www.jw.org/pt/publicacoes/livros/respostas-importantes/"
                },
                {
                    "id": "lvs",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/lvs/T/wpub/lvs_T_lg.jpg",
                    "name":"Continue a Amar a Deus",
                    "type": "b",
                    "link": "https://www.jw.org/pt/publicacoes/livros/continue-a-amar-a-deus/"
                },
                {
                    "id": "lfb",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/lfb/T/wpub/lfb_T_lg.jpg",
                    "name":"Aprenda com as Histórias da Bíblia",
                    "type": "b",
                    "link": "https://www.jw.org/pt/publicacoes/livros/aprenda-historias-biblia/"
                },
                {
                    "id": "co-invpw",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/co-invpw18/T/wpub/CO-invpw18_T_lg.jpg",
                    "name":"Convite do Congresso Regional de 2018",
                    "type": "inv",
                    "link": "https://download-a.akamaihd.net/files/media_books/2c/CO-invpw18_T.pdf"
                },
                {
                    "id": "ph",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/ph/T/wpub/ph_T_lg.jpg",
                    "name":"O Caminho para a Paz e Felicidade",
                    "type": "bl",
                    "link": "https://download-a.akamaihd.net/files/media_books/c0/ph_T.pdf"
                },
                {
                    "id": "ypq",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/ypq/T/wpub/ypq_T_lg.jpg",
                    "name":"10 Perguntas Que os Jovens Se Fazem e as Melhores Respostas",
                    "type": "bl",
                    "link": "https://www.jw.org/pt/publicacoes/livros/perguntas-dos-jovens/"
                },
                {
                    "id": "inv",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/inv/T/wpub/inv_T_lg.jpg",
                    "name":"Convite para reuniões congregacionais",
                    "type": "inv",
                    "link": "https://download-a.akamaihd.net/files/media_books/ed/inv_T.pdf"
                },
                {
                    "id": "hf",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/hf/T/wpub/hf_T_lg.jpg",
                    "name":"Você Pode Ter uma Família Feliz!",
                    "type": "bl",
                    "link": "https://www.jw.org/pt/publicacoes/livros/familia-feliz/"
                },
                {
                    "id": "t-34",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/t-34/T/wpub/T-34_T_lg.jpg",
                    "name":"O Sofrimento Vai Acabar Algum Dia?",
                    "type": "t",
                    "link": "https://www.jw.org/pt/publicacoes/livros/folheto-sofrimento-vai-acabar/"
                },
                {
                    "id": "t-33",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/t-33/T/wpub/T-33_T_lg.jpg",
                    "name":"Quem Controla o Mundo?",
                    "type": "t",
                    "link": "https://www.jw.org/pt/publicacoes/livros/folheto-quem-controla-o-mundo/"
                },
                {
                    "id": "t-32",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/t-32/T/wpub/T-32_T_lg.jpg",
                    "name":"Qual o Segredo para Ter uma Família Feliz?",
                    "type": "t",
                    "link": "Qual o Segredo para Ter uma Família Feliz?"
                },
                {
                    "id": "t-31",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/t-31/T/wpub/T-31_T_lg.jpg",
                    "name":"O Que Você Espera do Futuro?",
                    "type": "t",
                    "link": "https://www.jw.org/pt/publicacoes/livros/folheto-o-que-espera-do-futuro/"
                },
                {
                    "id": "t-30",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/t-30/T/wpub/T-30_T_lg.jpg",
                    "name":"O Que Você Acha da Bíblia?",
                    "type": "t",
                    "link": "https://www.jw.org/pt/publicacoes/livros/tratado-o-que-acha-da-biblia/"
                },
                {
                    "id": "t-35",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/t-35/T/wpub/T-35_T_lg.jpg",
                    "name":"Será Que os Mortos Podem Voltar a Viver?",
                    "type": "t",
                    "link": "https://www.jw.org/pt/publicacoes/livros/mortos-viver-novamente-folheto/"
                },
                {
                    "id": "fg",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/fg/T/wpub/fg_T_lg.jpg",
                    "name":"Boas Notícias de Deus para Você!",
                    "type": "bl",
                    "link": "https://www.jw.org/pt/publicacoes/livros/boas-noticias-de-deus-para-voce/"
                },
                {
                    "id": "jl",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/jl/T/wpub/jl_T_lg.jpg",
                    "name":"Quem Está Fazendo a Vontade de Jeová Hoje?",
                    "type": "bl",
                    "link": "https://www.jw.org/pt/publicacoes/livros/vontade-de-jeova/"
                },
                {
                    "id": "mb",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/mb/T/wpub/mb_T_lg.jpg",
                    "name":"Minhas Primeiras Lições da Bíblia",
                    "type": "bl",
                    "link": "https://download-a.akamaihd.net/files/media_books/5f/mb_T.pdf"
                },
                {
                    "id": "ld",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/ld/T/wpub/ld_T_lg.jpg",
                    "name":"Escute a Deus",
                    "type": "bl",
                    "link": "https://download-a.akamaihd.net/files/media_books/85/ld_T.pdf"
                },
                {
                    "id": "bm",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/bm/T/wpub/bm_T_lg.jpg",
                    "name":"A Bíblia — Qual É a Sua Mensagem?",
                    "type": "bl",
                    "link": "https://www.jw.org/pt/publicacoes/livros/mensagem-da-biblia/"
                },
                {
                    "id": "lc",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/lc/T/wpub/lc_T_lg.jpg",
                    "name":"A Vida — Teve um Criador?",
                    "type": "bl",
                    "link": "https://www.jw.org/pt/publicacoes/livros/vida-teve-criador/"
                },
                {
                    "id": "kt",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/kt/T/wpub/kt_T_lg.jpg",
                    "name":"Gostaria de conhecer a verdade?",
                    "type": "t",
                    "link": "https://www.jw.org/pt/publicacoes/livros/conhecer-verdade-folheto/"
                },
                {
                    "id": "ll",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/ll/T/wpub/ll_T_lg.jpg",
                    "name":"Escute a Deus e Viva para Sempre",
                    "type": "bl",
                    "link": "https://www.jw.org/pt/publicacoes/livros/escute-e-viva/"
                },
                {
                    "id": "fy",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/fy/T/wpub/fy_T_lg.jpg",
                    "name":"O Segredo de uma Família Feliz",
                    "type": "b",
                    "link": "https://download-a.akamaihd.net/files/media_books/72/fy_T.pdf"
                },
                {
                    "id": "bh",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/bh/T/wpub/bh_T_lg.jpg",
                    "name":"O Que a Bíblia Realmente Ensina?",
                    "type": "b",
                    "link": "https://www.jw.org/pt/publicacoes/livros/biblia-ensina/"
                },
                {
                    "id": "lr",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/lr/T/wpub/lr_T_lg.jpg",
                    "name":"Aprenda do Grande Instrutor",
                    "type": "b",
                    "link": "https://www.jw.org/pt/publicacoes/livros/aprenda-do-grande-instrutor-jesus/"
                },
                {
                    "id": "rj",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/rj/T/wpub/rj_T_lg.jpg",
                    "name":"Volte para Jeová",
                    "type": "bl",
                    "link": "https://www.jw.org/pt/publicacoes/livros/volte-para-jeova/"
                },
                {
                    "id": "jy",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/jy/T/wpub/jy_T_lg.jpg",
                    "name":"Jesus — o Caminho, a Verdade e a Vida",
                    "type": "b",
                    "link": "https://www.jw.org/pt/publicacoes/livros/Jesus-o-Caminho-a-Verdade-e-a-Vida/"
                },
                {
                    "id": "yc",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/yc/T/wpub/yc_T_lg.jpg",
                    "name":"Ensine Seus Filhos",
                    "type": "bl",
                    "link": "https://www.jw.org/pt/publicacoes/livros/ensine-seus-filhos/"
                },
                {
                    "id": "lf",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/lf/T/wpub/lf_T_lg.jpg",
                    "name":"A origem da vida — Cinco perguntas que merecem resposta",
                    "type": "bl",
                    "link": "https://www.jw.org/pt/publicacoes/livros/origem-da-vida-5-perguntas/"
                },
                {
                    "id": "dg",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/dg/T/wpub/dg_T_lg.jpg",
                    "name":"Importa-se Deus Realmente Conosco?",
                    "type": "bl",
                    "link": "https://download-a.akamaihd.net/files/media_books/98/dg_T.pdf"
                },
                {
                    "id": "sp",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/sp/T/wpub/sp_T_lg.jpg",
                    "name":"Espíritos dos Mortos — Ajudam? Ou Prejudicam? Existem Realmente?",
                    "type": "bl",
                    "link": "https://www.jw.org/pt/publicacoes/livros/espiritos-dos-mortos/"
                },
                {
                    "id": "hl",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/hl/T/wpub/hl_T_lg.jpg",
                    "name":"Como Você Pode Ter uma Vida Feliz?",
                    "type": "bl",
                    "link": "https://www.jw.org/pt/publicacoes/livros/familia-feliz-2/"
                },
                {
                    "id": "yi",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/yi/T/wpub/yi_T_lg.jpg",
                    "name":"Jovens — Como Usarão Sua Vida?",
                    "type": "bl",
                    "link": "https://download-a.akamaihd.net/files/media_books/cc/yi_T.pdf"
                },
                {
                    "id": "pc",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/pc/T/wpub/pc_T_lg.jpg",
                    "name":"Como Ter Verdadeira Paz e Felicidade",
                    "type": "bl",
                    "link": "https://www.jw.org/pt/publicacoes/livros/verdadeira-paz-e-felicidade/"
                },
                {
                    "id": "ol",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/ol/T/wpub/ol_T_lg.jpg",
                    "name":"O Caminho para a Vida Eterna — Já o Encontrou?",
                    "type": "bl",
                    "link": "https://www.jw.org/pt/publicacoes/livros/caminho-para-vida/"
                },
                {
                    "id": "yp2",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/yp2/T/wpub/yp2_T_lg.jpg",
                    "name":"Os Jovens Perguntam — Respostas Práticas, Volume 2",
                    "type": "b",
                    "link": "https://download-a.akamaihd.net/files/media_books/31/yp2_T.pdf"
                },
                {
                    "id": "yp1",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/yp1/T/wpub/yp1_T_lg.jpg",
                    "name":"Os Jovens Perguntam — Respostas Práticas, Volume 1",
                    "type": "b",
                    "link": "https://download-a.akamaihd.net/files/media_books/14/yp1_T.pdf"
                },
                {
                    "id": "rk",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/rk/T/wpub/rk_T_lg.jpg",
                    "name":"Verdadeira Fé — O Segredo de uma Vida Feliz",
                    "type": "bl",
                    "link": "https://www.jw.org/pt/publicacoes/livros/verdadeira-fe-2/"
                },
                {
                    "id": "gf",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/gf/T/wpub/gf_T_lg.jpg",
                    "name":"Poderá Ser Amigo de Deus!",
                    "type": "bl",
                    "link": "https://www.jw.org/pt/publicacoes/livros/amigo-de-deus/"
                },
                {
                    "id": "gt",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/gt/T/wpub/gt_T_lg.jpg",
                    "name":"O Maior Homem Que Já Viveu",
                    "type": "b",
                    "link": "https://download-a.akamaihd.net/files/media_books/7b/gt_T.pdf"
                },
                {
                    "id": "my",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/my/T/wpub/my_T_lg.jpg",
                    "name":"Meu Livro de Histórias Bíblicas",
                    "type": "b",
                    "link": "https://www.jw.org/pt/publicacoes/livros/historias-biblicas/"
                },
                {
                    "id": "we",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/we/T/wpub/we_T_lg.jpg",
                    "name":"Quando Morre Alguém Que Amamos",
                    "type": "bl",
                    "link": "https://www.jw.org/pt/publicacoes/livros/quando-morre-alguem/"
                },
                {
                    "id": "la",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/la/T/wpub/la_T_lg.jpg",
                    "name":"Como Ter uma Vida Satisfatória",
                    "type": "bl",
                    "link": "https://www.jw.org/pt/publicacoes/livros/vida-satisfatoria/"
                },
                {
                    "id": "lv",
                    "url":"https://assetsnffrgf-a.akamaihd.net/assets/a/lv/T/wpub/lv_T_lg.jpg",
                    "name":"Mantenha-se no Amor de Deus",
                    "type": "b",
                    "link": "https://www.jw.org/pt/publicacoes/l‘Mantenha-se no Amor de Deus’ivros/amor-de-deus/"
                }
            ]
        }
        property var publicationsType: []

        onListDownloadChanged: {
            if (typeof listDownload.downloading !== "undefined")
                listModelDownload.setProperty(listDownload.index, "downloading", listDownload.downloading)
            if (typeof listDownload.file !== "undefined") {
                listModelDownload.setProperty(listDownload.index, "file", listDownload.file)
                if (listDownload.file !== "")
                    downloadManager.savePublication(listDownload.index)
            }
        }

        function savePublication(index) {
            listModelDownload.setProperty(index, "downloaded", true)

            var object = {}
            object['code'] = listModelDownload.get(index).id
            object['name'] = listModelDownload.get(index).name
            object['file'] = listModelDownload.get(index).file
            object['link'] = listModelDownload.get(index).link
            object['pubTypeId'] = publicationsType[listModelDownload.get(index).type] !== undefined? publicationsType[listModelDownload.get(index).type] : 0

            DB.add("Publications", object)
            listPublications.getRows()
        }
    }

    onClosed: closeDialog()

    Component.onCompleted: {
        padding = 0

        for (var t in downloadManager.publications.types) {
            var exist = DB.getOne("PublicationsType", "type='" + downloadManager.publications.types[t].type + "'")
            if (!Object.keys(exist).length)
                DB.add("PublicationsType", downloadManager.publications.types[t])
        }
        listPublicationsType.getRows()

        for (var it = 0; it < listPublicationsType.count; ++it)
            downloadManager.publicationsType[listPublicationsType.get(it).type] = listPublicationsType.get(it).id

        for (var k in downloadManager.publications.publications) {
            var publication = DB.getOne("Publications", "code='" + downloadManager.publications.publications[k].id + "'")
            if (Object.keys(publication).length)
                continue;

            downloadManager.publications.publications[k].downloaded = false
            listModelDownload.append(downloadManager.publications.publications[k]);
            downloadManager.addItemDownload({
                "id": downloadManager.publications.publications[k].id,
                "url": downloadManager.publications.publications[k].url,
                "index": listModelDownload.count - 1
            });
        }
    }
}
