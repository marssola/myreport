import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.4

import "../functions/Utils.js" as Util
import "../Components"

Popup {
    id: dialog

    width: window.width < 300? window.width - 10 : 300
    height: window.height < 250? window.height : 250

    x: (window.width - width) / 2
    y: (window.height - height) / 2

    onClosed: closeDialog()
    parent: ApplicationWindow.overlay
    modal: true

    Flickable {
        anchors.fill: parent
        contentHeight: column.implicitHeight
        clip: true

        Column {
            id: column
            width: parent.width

            ComboBox {
                width: parent.width
                flat: true
                model: [2017, 2018, 2019]

                currentIndex: {
                    for (var key in model) {
                        if (model[key] === object.date.getFullYear())
                            return key
                    }
                    return -1
                }

                onCurrentIndexChanged: {
                    var d = object.date
                    d.setFullYear(model[currentIndex])
                    object.date = d
                }
            }

            Grid {
                id: grid
                width: parent.width
                columns: 3

                Repeater {
                    model: 12

                    ItemDelegate {
                        width: grid.width / grid.columns
                        height: 40
                        highlighted: modelData === object.date.getMonth()

                        contentItem: Label {
                            text: Util.getMonth(modelData, 3)
                            font.capitalization: Font.AllUppercase
                            horizontalAlignment: Label.AlignHCenter
                            verticalAlignment: Label.AlignVCenter
                        }

                        onClicked: {
                            var d = object.date
                            d.setMonth(modelData)
                            object.date = d
                            dialog.close()
                        }
                    }
                }
            }
        }
    }
}
