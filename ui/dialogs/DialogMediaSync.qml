import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.4
import QtQuick.LocalStorage 2.0

import com.Utils.DownloadManager 0.1
import "../functions/Database.js" as DB
import "../Components"

Dialog {
    id: dialogDownloadManager

    title: qsTr("Atualizar Vídeos")
    width: window.width < 400? window.width - 10 : 400
    height: window.height < 600? window.height - 10 : 600
    x: (window.width - width) / 2
    y: (window.height - height) / 2

    modal: true
    parent: ApplicationWindow.overlay
    standardButtons: Dialog.Close


    ColumnLayout {
        anchors.fill: parent

        ColumnLayout {
            visible: !listModelDownload.count
            Layout.fillHeight: true
            Layout.fillWidth: true

            Label {
                Layout.fillWidth: true
                Layout.fillHeight: true
                text: "\uf59c"
                font.family: fa.name
                font.pixelSize: 30
                opacity: 0.5

                verticalAlignment: Label.AlignBottom
                horizontalAlignment: Label.AlignHCenter
            }
            Label {
                Layout.fillHeight: true
                Layout.fillWidth: true
                text: qsTr("Não há nada para atualizar")
                horizontalAlignment: Label.AlignHCenter
                opacity: 0.5
            }
        }

        ListModel {
            id: listModelDownload
        }

        ListView {
            id: listViewDownload

            Layout.fillHeight: true
            Layout.fillWidth: true
            clip: true
            visible: listModelDownload.count

            model: listModelDownload
            delegate: ItemDelegate {
                width: parent.width
                height: 70

                contentItem: RowLayout {
                    anchors.fill: parent

                    Item {
                        Layout.preferredWidth: height
                        Layout.fillHeight: true

                        Layout.margins: 2

                        BusyIndicator {
                            anchors.fill: parent
                            padding: 25
                            running: imgMedia.status === Image.Loading
                        }

                        Label {
                            visible: imgMedia.status === Image.Null || imgMedia.status === Image.Error
                            anchors.fill: parent
                            verticalAlignment: Label.AlignVCenter
                            horizontalAlignment: Label.AlignHCenter

                            text: "\uf008"
                            color: Material.foreground
                            opacity: 0.5
                            font.family: fa.name
                            font.pixelSize: height * 0.5
                        }

                        Image {
                            id: imgMedia
                            visible: Image.Ready

                            source: file === "" || !file? "" : "file://" + file
                            width: parent.width
                            height: parent.height
                            anchors.centerIn: parent
                            fillMode: Image.PreserveAspectCrop

                            Rectangle {
                                width: 20
                                height: width

                                anchors.right: parent.right
                                anchors.rightMargin: 2
                                anchors.bottom: parent.bottom
                                anchors.bottomMargin: 4

                                opacity: 0.7
                                radius: width

                                color: Material.background

                                Label {
                                    text: "\uf03d"
                                    font.family: fa.name
                                    anchors.centerIn: parent
                                    font.pixelSize: parent.width * 0.5
                                }
                            }
                        }
                    }

                    ColumnLayout {
                        Layout.fillHeight: true
                        Layout.fillWidth: true

                        Label {
                            Layout.fillHeight: true
                            Layout.fillWidth: true
                            Layout.margins: 2

                            text: name
                            wrapMode: Label.WordWrap
                            elide: Label.ElideRight
                            verticalAlignment: Label.AlignVCenter
                        }

                        Label {
                            Layout.fillWidth: true
                            Layout.margins: 5
                            text: time
                            font.pixelSize: 12
                            horizontalAlignment: Label.AlignRight
                            opacity: 0.5
                        }
                    }

                    Label {
                        Layout.fillHeight: true
                        verticalAlignment: Label.AlignBottom
                        padding: 5

                        text: !downloaded? "\uf381" : "\uf058"
                        font.family: fa.name
                        color: Material.foreground
                        opacity: 0.6
                    }
                }

                ProgressBar {
                    id: progressBottom
                    visible: !downloaded
                    width: parent.width
                    anchors.bottom: parent.bottom
                    indeterminate: !value
                    value: typeof downloading === 'undefined'? 0.0 : downloading
                }

                Rectangle {
                    visible: downloaded
                    width: parent.width
                    height: 1
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 1
                    color: object.border_up
                }

                Rectangle {
                    visible: downloaded
                    width: parent.width
                    height: 1
                    anchors.bottom: parent.bottom
                    color: object.border_down
                }
            }
        }
    }

    DownloadManager {
        id: downloadManager

        property var media: {
            "types": [
                        {
                            "type": 'children',
                            "name": 'Crianças',
                        },
                        {
                            "type": "bjf",
                            "name": "Torne-se Amigo de Jeová"
                        },
                        {
                            "type": 'whiteboard',
                            "name": 'Animações no Quadro Branco',
                        },
                        {
                            "type": 'teenagers',
                            "name": 'Adolescentes',
                        },
                        {
                            "type": 'family',
                            "name": 'Família',
                        },
                        {
                            "type": 'ministry',
                            "name": 'Nossas Reuniões e Obra de Pregação',
                        },
                        {
                            "type": 'organization',
                            "name": 'Nossa Organização',
                        },
                        {
                            "type": 'bibleteachings',
                            "name": 'Ensinos da Bíblia',
                        },
                        {
                            "type": 'biblecreation',
                            "name": 'Criação',
                        },
                        {
                            "type": 'filmes',
                            "name": 'Filmes',
                        }
                    ],
                    "media": [
                        {
                            "id": "pub-pkon_11_VIDEO",
                            "url": "https://assetsnffrgf-a.akamaihd.net/assets/m/502018385/univ/art/502018385_univ_wss_lg.jpg",
                            "name": "Cuide da casa de Jeová",
                            "type": "bjf",
                            "link": "https://www.jw.org/pt/publicacoes/videos/#pt/mediaitems/BJF/pub-pk_31_VIDEO",
                            "time": "1:15"
                        },
                        {
                            "id": "pub-pk_30_VIDEO",
                            "url": "https://assetsnffrgf-a.akamaihd.net/assets/m/502018101/univ/art/502018101_univ_wss_lg.jpg",
                            "name": "Mesmo triste, não desista",
                            "type": "bjf",
                            "link": "https://www.jw.org/pt/publicacoes/videos/#pt/mediaitems/BJF/pub-pk_30_VIDEO",
                            "time": "1:39"
                        },
                        {
                            "id": "pub-pk_29_VIDEO",
                            "url": "https://assetsnffrgf-a.akamaihd.net/assets/m/502017197/univ/art/502017197_univ_wss_lg.jpg",
                            "name": "Seja humilde",
                            "type": "bjf",
                            "link": "https://www.jw.org/pt/publicacoes/videos/#pt/mediaitems/BJF/pub-pk_29_VIDEO",
                            "time": "2:08"
                        },
                        {
                            "id": "pub-pk_28_VIDEO",
                            "url": "https://assetsnffrgf-a.akamaihd.net/assets/m/502017207/univ/art/502017207_univ_wss_lg.jpg",
                            "name": "Não desista mesmo quando te tratarem mal",
                            "type": "bjf",
                            "link": "https://www.jw.org/pt/publicacoes/videos/#pt/mediaitems/BJF/pub-pk_28_VIDEO",
                            "time": "1:31"
                        },
                        {
                            "id": "pub-pk_27_VIDEO",
                            "url": "https://assetsnffrgf-a.akamaihd.net/assets/m/502017228/univ/art/502017228_univ_wss_lg.jpg",
                            "name": "Imagine-se no Paraíso",
                            "type": "bjf",
                            "link": "https://www.jw.org/pt/publicacoes/videos/#pt/mediaitems/BJF/pub-pk_27_VIDEO",
                            "time": "1:50"
                        },
                        {
                            "id": "pub-pk_26_VIDEO",
                            "url": "https://assetsnffrgf-a.akamaihd.net/assets/m/502017163/univ/art/502017163_univ_wss_lg.jpg",
                            "name": "O resgate",
                            "type": "bjf",
                            "link": "https://www.jw.org/pt/publicacoes/videos/#pt/mediaitems/BJF/pub-pk_26_VIDEO",
                            "time": "2:07"
                        },
                        {
                            "id": "pub-pk_25_VIDEO",
                            "url": "https://assetsnffrgf-a.akamaihd.net/assets/m/502016285/univ/art/502016285_univ_wss_lg.jpg",
                            "name": "Faça novos amigos!",
                            "type": "bjf",
                            "link": "https://www.jw.org/pt/publicacoes/videos/#pt/mediaitems/BJF/pub-pk_25_VIDEO",
                            "time": "2:06"
                        },
                        {
                            "id": "pub-pk_23_VIDEO",
                            "url": "https://assetsnffrgf-a.akamaihd.net/assets/m/502016277/univ/art/502016277_univ_wss_lg.jpg",
                            "name": "O nome de Jeová",
                            "type": "bjf",
                            "link": "https://www.jw.org/pt/publicacoes/videos/#pt/mediaitems/BJF/pub-pk_23_VIDEO",
                            "time": "1:54"
                        },
                        {
                            "id": "pub-pk_24_VIDEO",
                            "url": "https://assetsnffrgf-a.akamaihd.net/assets/m/502016276/univ/art/502016276_univ_wss_lg.jpg",
                            "name": "Jeová fez tudo bonito!",
                            "type": "bjf",
                            "link": "https://www.jw.org/pt/publicacoes/videos/#pt/mediaitems/BJF/pub-pk_24_VIDEO",
                            "time": "2:01"
                        },
                        {
                            "id": "pub-pk_22_VIDEO",
                            "url": "https://assetsnffrgf-a.akamaihd.net/assets/m/502016246/univ/art/502016246_univ_wss_lg.jpg",
                            "name": "Um homem e uma mulher",
                            "type": "bjf",
                            "link": "https://www.jw.org/pt/publicacoes/videos/#pt/mediaitems/BJF/pub-pk_22_VIDEO",
                            "time": "2:22"
                        },
                        {
                            "id": "pub-pk_21_VIDEO",
                            "url": "https://assetsnffrgf-a.akamaihd.net/assets/m/502016236/univ/art/502016236_univ_wss_lg.jpg",
                            "name": "Tenha mais paciência!",
                            "type": "bjf",
                            "link": "https://www.jw.org/pt/publicacoes/videos/#pt/mediaitems/BJF/pub-pk_21_VIDEO",
                            "time": "2:14"
                        },
                        {
                            "id": "pub-pkon_5_VIDEO",
                            "url": "https://assetsnffrgf-a.akamaihd.net/assets/m/502016119/univ/art/502016119_univ_wss_lg.jpg",
                            "name": "Sempre agradeça",
                            "type": "bjf",
                            "link": "https://www.jw.org/pt/publicacoes/videos/#pt/mediaitems/BJF/pub-pkon_5_VIDEO",
                            "time": "1:19"
                        },
                        {
                            "id": "pub-pk_19_VIDEO",
                            "url": "https://assetsnffrgf-a.akamaihd.net/assets/m/502015236/univ/art/502015236_univ_wss_lg.jpg",
                            "name": "Seja generoso",
                            "type": "bjf",
                            "link": "https://www.jw.org/pt/publicacoes/videos/#pt/mediaitems/BJF/pub-pk_19_VIDEO",
                            "time": "1:15"
                        },
                        {
                            "id": "pub-pk_20_VIDEO",
                            "url": "https://assetsnffrgf-a.akamaihd.net/assets/m/502015275/univ/art/502015275_univ_wss_lg.jpg",
                            "name": "Fale a verdade",
                            "type": "bjf",
                            "link": "https://www.jw.org/pt/publicacoes/videos/#pt/mediaitems/BJF/pub-pk_20_VIDEO",
                            "time": "1:44"
                        },
                        {
                            "id": "pub-pk_18_VIDEO",
                            "url": "https://assetsnffrgf-a.akamaihd.net/assets/m/502015278/univ/art/502015278_univ_wss_lg.jpg",
                            "name": "Respeite a casa de Jeová",
                            "type": "bjf",
                            "link": "https://www.jw.org/pt/publicacoes/videos/#pt/mediaitems/BJF/pub-pk_18_VIDEO",
                            "time": "1:58"
                        },
                        {
                            "id": "pub-pk_17_VIDEO",
                            "url": "https://assetsnffrgf-a.akamaihd.net/assets/m/502015265/univ/art/502015265_univ_wss_lg.jpg",
                            "name": "Proteja seus filhos",
                            "type": "bjf",
                            "link": "https://www.jw.org/pt/publicacoes/videos/#pt/mediaitems/BJF/pub-pk_17_VIDEO",
                            "time": "1:52"
                        },
                        {
                            "id": "pub-pk_16_VIDEO",
                            "url": "https://assetsnffrgf-a.akamaihd.net/assets/m/502015209/univ/art/502015209_univ_wss_lg.jpg",
                            "name": "Pregue em outro idioma",
                            "type": "bjf",
                            "link": "https://www.jw.org/pt/publicacoes/videos/#pt/mediaitems/BJF/pub-pk_16_VIDEO",
                            "time": "1:38"
                        },
                        {
                            "id": "pub-pk_15_VIDEO",
                            "url": "https://assetsnffrgf-a.akamaihd.net/assets/m/502015169/univ/art/502015169_univ_wss_lg.jpg",
                            "name": "Preste atenção nas reuniões",
                            "type": "bjf",
                            "link": "https://www.jw.org/pt/publicacoes/videos/#pt/mediaitems/BJF/pub-pk_15_VIDEO",
                            "time": "1:33"
                        },
                        {
                            "id": "pub-pk_14_VIDEO",
                            "url": "https://assetsnffrgf-a.akamaihd.net/assets/m/502014351/univ/art/502014351_univ_wss_lg.jpg",
                            "name": "Prepare seu comentário",
                            "type": "bjf",
                            "link": "https://www.jw.org/pt/publicacoes/videos/#pt/mediaitems/BJF/pub-pk_14_VIDEO",
                            "time": "1:52"
                        },
                        {
                            "id": "pub-pk_13_VIDEO",
                            "url": "https://assetsnffrgf-a.akamaihd.net/assets/m/1102014289/univ/art/1102014289_univ_wss_lg.jpg",
                            "name": "Jeová ajudará você a ter coragem",
                            "type": "bjf",
                            "link": "https://www.jw.org/pt/publicacoes/videos/#pt/mediaitems/BJF/pub-pk_13_VIDEO",
                            "time": "11:59"
                        },
                        {
                            "id": "pub-pk_12_VIDEO",
                            "url": "https://assetsnffrgf-a.akamaihd.net/assets/m/1102014323/univ/art/1102014323_univ_wss_lg.jpg",
                            "name": "Pedro e Sofia visitam Betel",
                            "type": "bjf",
                            "link": "https://www.jw.org/pt/publicacoes/videos/#pt/mediaitems/BJF/pub-pk_12_VIDEO",
                            "time": "1:46"
                        },
                        {
                            "id": "pub-pk_900_VIDEO",
                            "url": "https://assetsnffrgf-a.akamaihd.net/assets/m/502014319/univ/art/502014319_univ_wss_lg.jpg",
                            "name": "Uma série que conquistou as crianças",
                            "type": "bjf",
                            "link": "https://www.jw.org/pt/publicacoes/videos/#pt/mediaitems/BJF/pub-pk_900_VIDEO",
                            "time": "4:48"
                        },
                        {
                            "id": "pub-pk_11_VIDEO",
                            "url": "https://assetsnffrgf-a.akamaihd.net/assets/m/1102014282/univ/art/1102014282_univ_wss_lg.jpg",
                            "name": "Aprenda a perdoar",
                            "type": "bjf",
                            "link": "https://www.jw.org/pt/publicacoes/videos/#pt/mediaitems/BJF/pub-pk_11_VIDEO",
                            "time": "2:15"
                        },
                        {
                            "id": "pub-pk_10_VIDEO",
                            "url": "https://assetsnffrgf-a.akamaihd.net/assets/m/1102014281/univ/art/1102014281_univ_wss_lg.jpg",
                            "name": "Divida suas coisas com outros",
                            "type": "bjf",
                            "link": "https://www.jw.org/pt/publicacoes/videos/#pt/mediaitems/BJF/pub-pk_10_VIDEO",
                            "time": "2:03"
                        },
                        {
                            "id": "pub-pk_9_VIDEO",
                            "url": "https://assetsnffrgf-a.akamaihd.net/assets/m/1102014280/univ/art/1102014280_univ_wss_lg.jpg",
                            "name": "Jeová criou todas as coisas",
                            "type": "bjf",
                            "link": "https://www.jw.org/pt/publicacoes/videos/#pt/mediaitems/BJF/pub-pk_9_VIDEO",
                            "time": "2:36"
                        },
                        {
                            "id": "pub-pk_8_VIDEO",
                            "url": "https://assetsnffrgf-a.akamaihd.net/assets/m/1102013549/univ/art/1102013549_univ_wss_lg.jpg",
                            "name": "Seja limpo e organizado",
                            "type": "bjf",
                            "link": "https://www.jw.org/pt/publicacoes/videos/#pt/mediaitems/BJF/pub-pk_8_VIDEO",
                            "time": "2:16"
                        },
                        {
                            "id": "pub-pk_7_VIDEO",
                            "url": "https://assetsnffrgf-a.akamaihd.net/assets/m/1102013546/univ/art/1102013546_univ_wss_lg.jpg",
                            "name": "Dar de coração traz alegria",
                            "type": "bjf",
                            "link": "https://www.jw.org/pt/publicacoes/videos/#pt/mediaitems/BJF/pub-pk_7_VIDEO",
                            "time": "1:26"
                        },
                        {
                            "id": "pub-pk_6_VIDEO",
                            "url": "https://assetsnffrgf-a.akamaihd.net/assets/m/1102013428/univ/art/1102013428_univ_wss_lg.jpg",
                            "name": "Por favor e obrigado",
                            "type": "bjf",
                            "link": "https://www.jw.org/pt/publicacoes/videos/#pt/mediaitems/BJF/pub-pk_6_VIDEO",
                            "time": "1:42"
                        },
                        {
                            "id": "pub-pk_5_VIDEO",
                            "url": "https://assetsnffrgf-a.akamaihd.net/assets/m/1102013541/univ/art/1102013541_univ_wss_lg.jpg",
                            "name": "Vamos pregar",
                            "type": "bjf",
                            "link": "https://www.jw.org/pt/publicacoes/videos/#pt/mediaitems/BJF/pub-pk_5_VIDEO",
                            "time": "1:22"
                        },
                        {
                            "id": "pub-pk_4_VIDEO",
                            "url": "https://assetsnffrgf-a.akamaihd.net/assets/m/1102013336/univ/art/1102013336_univ_wss_lg.jpg",
                            "name": "É errado roubar",
                            "type": "bjf",
                            "link": "https://www.jw.org/pt/publicacoes/videos/#pt/mediaitems/BJF/pub-pk_4_VIDEO",
                            "time": "1:23"
                        },
                        {
                            "id": "pub-pk_3_VIDEO",
                            "url": "https://assetsnffrgf-a.akamaihd.net/assets/m/1102013351/univ/art/1102013351_univ_wss_lg.jpg",
                            "name": "Sempre fale com Jeová",
                            "type": "bjf",
                            "link": "https://www.jw.org/pt/publicacoes/videos/#pt/mediaitems/BJF/pub-pk_3_VIDEO",
                            "time": "1:22"
                        },
                        {
                            "id": "pub-pk_2_VIDEO",
                            "url": "https://assetsnffrgf-a.akamaihd.net/assets/m/1102013335/univ/art/1102013335_univ_wss_lg.jpg",
                            "name": "Obedeça a Jeová",
                            "type": "bjf",
                            "link": "https://www.jw.org/pt/publicacoes/videos/#pt/mediaitems/BJF/pub-pk_2_VIDEO",
                            "time": "2:59"
                        },
                        {
                            "id": "pub-pk_1_VIDEO",
                            "url": "https://assetsnffrgf-a.akamaihd.net/assets/m/1102013334/univ/art/1102013334_univ_wss_lg.jpg",
                            "name": "Obedeça a seus pais",
                            "type": "bjf",
                            "link": "https://www.jw.org/pt/publicacoes/videos/#pt/mediaitems/BJF/pub-pk_1_VIDEO",
                            "time": "6:10"
                        },
                        {
                            "id": "pub-pk_0_VIDEO",
                            "url": "https://assetsnffrgf-a.akamaihd.net/assets/m/1102013333/univ/art/1102013333_univ_wss_lg.jpg",
                            "name": "Uma mensagem para sua família",
                            "type": "bjf",
                            "link": "https://www.jw.org/pt/publicacoes/videos/#pt/mediaitems/BJF/pub-pk_0_VIDEO",
                            "time": "2:11"
                        },
                        {
                            "id": "docid-502018215_1_VIDEO",
                            "url": "https://assetsnffrgf-a.akamaihd.net/assets/m/502018215/univ/art/502018215_univ_wss_lg.jpg",
                            "name": "Esportes — Com estas dicas a diversão é garantida!",
                            "type": "whiteboard",
                            "link": "https://www.jw.org/pt/publicacoes/videos/#pt/mediaitems/TeenWhiteboardAnimations/docid-502018215_1_VIDEO",
                            "time": "2:26"
                        },
                        {
                            "id": "docid-502017872_1_VIDEO",
                            "url": "https://assetsnffrgf-a.akamaihd.net/assets/m/502017872/univ/art/502017872_univ_wss_lg.jpg",
                            "name": "Pare e pense: devo beber?",
                            "type": "whiteboard",
                            "link": "https://www.jw.org/pt/publicacoes/videos/#pt/mediaitems/TeenWhiteboardAnimations/docid-502017872_1_VIDEO",
                            "time": "2:31"
                        },
                        {
                            "id": "docid-502017178_1_VIDEO",
                            "url": "https://assetsnffrgf-a.akamaihd.net/assets/m/502017178/univ/art/502017178_univ_wss_lg.jpg",
                            "name": "Conversar com os meus pais? Como?",
                            "type": "whiteboard",
                            "link": "https://www.jw.org/pt/publicacoes/videos/#pt/mediaitems/TeenWhiteboardAnimations/docid-502017178_1_VIDEO",
                            "time": "2:19"
                        },
                        {
                            "id": "docid-502016296_1_VIDEO",
                            "url": "https://assetsnffrgf-a.akamaihd.net/assets/m/502016296/univ/art/502016296_univ_wss_lg.jpg",
                            "name": "Celular e você: quem manda em quem?",
                            "type": "whiteboard",
                            "link": "https://www.jw.org/pt/publicacoes/videos/#pt/mediaitems/TeenWhiteboardAnimations/docid-502016296_1_VIDEO",
                            "time": "2:12"
                        },
                        {
                            "id": "docid-502016296_1_VIDEO",
                            "url": "https://assetsnffrgf-a.akamaihd.net/assets/m/502016272/univ/art/502016272_univ_wss_lg.jpg",
                            "name": "Como ganhar mais liberdade?",
                            "type": "whiteboard",
                            "link": "https://www.jw.org/pt/publicacoes/videos/#pt/mediaitems/TeenWhiteboardAnimations/docid-502016272_1_VIDEO",
                            "time": "2:48"
                        },
                        {
                            "id": "docid-502016241_1_VIDEO",
                            "url": "https://assetsnffrgf-a.akamaihd.net/assets/m/502016241/univ/art/502016241_univ_wss_lg.jpg",
                            "name": "Fofoca? Sai dessa!",
                            "type": "whiteboard",
                            "link": "https://www.jw.org/pt/publicacoes/videos/#pt/mediaitems/TeenWhiteboardAnimations/docid-502016241_1_VIDEO",
                            "time": "2:36"
                        },
                        {
                            "id": "docid-502016211_1_VIDEO",
                            "url": "https://assetsnffrgf-a.akamaihd.net/assets/m/502016211/univ/art/502016211_univ_wss_lg.jpg",
                            "name": "Será que é amor ou paixão?",
                            "type": "whiteboard",
                            "link": "https://www.jw.org/pt/publicacoes/videos/#pt/mediaitems/TeenWhiteboardAnimations/docid-502016211_1_VIDEO",
                            "time": "3:06"
                        },
                        {
                            "id": "docid-502015143_1_VIDEO",
                            "url": "https://assetsnffrgf-a.akamaihd.net/assets/m/502015143/univ/art/502015143_univ_wss_lg.jpg",
                            "name": "Diga não à pressão dos colegas!",
                            "type": "whiteboard",
                            "link": "https://www.jw.org/pt/publicacoes/videos/#pt/mediaitems/TeenWhiteboardAnimations/docid-502015143_1_VIDEO",
                            "time": "4:00"
                        },
                        {
                            "id": "docid-502014276_1_VIDEO",
                            "url": "https://assetsnffrgf-a.akamaihd.net/assets/m/502014276/univ/art/502014276_univ_wss_lg.jpg",
                            "name": "Seja esperto: saiba usar as redes sociais",
                            "type": "whiteboard",
                            "link": "https://www.jw.org/pt/publicacoes/videos/#pt/mediaitems/TeenWhiteboardAnimations/docid-502014276_1_VIDEO",
                            "time": "4:12"
                        },
                        {
                            "id": "docid-502013393_1_VIDEO",
                            "url": "https://assetsnffrgf-a.akamaihd.net/assets/m/502013393/univ/art/502013393_univ_wss_lg.jpg",
                            "name": "O que é um amigo de verdade?",
                            "type": "whiteboard",
                            "link": "https://www.jw.org/pt/publicacoes/videos/#pt/mediaitems/TeenWhiteboardAnimations/docid-502013393_1_VIDEO",
                            "time": "4:14"
                        },
                        {
                            "id": "docid-502013189_1_VIDEO",
                            "url": "https://assetsnffrgf-a.akamaihd.net/assets/m/502013189/univ/art/502013189_univ_wss_lg.jpg",
                            "name": "Como enfrentar o bullying sem partir para a briga",
                            "type": "whiteboard",
                            "link": "https://www.jw.org/pt/publicacoes/videos/#pt/mediaitems/TeenWhiteboardAnimations/docid-502013189_1_VIDEO",
                            "time": "3:52"
                        },
                    ]
        }
        property var mediaType: []

        onListDownloadChanged: {
            if (typeof listDownload.downloading !== "undefined")
                listModelDownload.setProperty(listDownload.index, "downloading", listDownload.downloading)
            if (typeof listDownload.file !== "undefined") {
                listModelDownload.setProperty(listDownload.index, "file", listDownload.file)
                if (listDownload.file !== "")
                    downloadManager.saveMedia(listDownload.index)
            }
        }

        function saveMedia(index) {
            listModelDownload.setProperty(index, "downloaded", true)

            var object = {}
            object['code'] = listModelDownload.get(index).id
            object['name'] = listModelDownload.get(index).name
            object['file'] = listModelDownload.get(index).file
            object['link'] = listModelDownload.get(index).link
            object['time'] = listModelDownload.get(index).time
            object['mediaTypeId'] = mediaType[listModelDownload.get(index).type] !== undefined? mediaType[listModelDownload.get(index).type] : 0

            DB.add("Media", object)
            listMedia.getRows()
        }
    }

    onClosed: closeDialog()

    onVisibleChanged: {
        if (visible) {
            for (var t in downloadManager.media.types) {
                var exist = DB.getOne("MediaType", "type='" + downloadManager.media.types[t].type + "'")
                if (!Object.keys(exist).length)
                    DB.add("MediaType", downloadManager.media.types[t])
            }
            listMediaType.getRows()

            for (var it = 0; it < listMediaType.count; ++it)
                downloadManager.mediaType[listMediaType.get(it).type] = listMediaType.get(it).id

            for (var m in downloadManager.media.media) {
                var media = DB.getOne("Media", "code='" + downloadManager.media.media[m].id + "'")
                if (Object.keys(media).length)
                    continue;

                downloadManager.media.media[m].downloaded = false
                listModelDownload.append(downloadManager.media.media[m])
                downloadManager.addItemDownload({
                    "id": downloadManager.media.media[m].id,
                    "url": downloadManager.media.media[m].url,
                    "index": listModelDownload.count - 1
                })
            }
        }
    }

    Component.onCompleted: {
        padding = 0
    }
}
