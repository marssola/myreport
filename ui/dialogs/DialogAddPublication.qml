import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.4
import QtQuick.LocalStorage 2.0

import "../functions/Database.js" as DB
import "../Components"

Dialog {
    id: dialog

    title: qsTr("Adicionar publicação")

    property int publicationId
    onPublicationIdChanged: {
        if (publicationId < 1)
            return

        var publication = DB.get("Publications", "id=" + publicationId)
        if (publication.length) {
            publicationName.text = publication.item(0).name
            for (var k = 0; k < listPublicationsType.count; ++k)
                if (publication.item(0).pubTypeId === listPublicationsType.get(k).id)
                    publicationType.currentIndex = k
        } else {
            publicationId = undefined
        }
    }

    width: window.width < 400? window.width - 10 : 400
    height: window.height < 280? window.height : 280
    parent: ApplicationWindow.overlay
    x: (window.width - width) / 2
    y: (window.height - height) / 2

    modal: true
    closePolicy: Popup.NoAutoClose

    Flickable {
        anchors.fill: parent
        contentHeight: column.implicitHeight
        clip: true

        Column {
            id: column
            width: parent.width

            Item {
                width: parent.width
                height: 15
            }

            Label {
                width: parent.width
                text: qsTr("Nome")
                font.bold: true
            }
            TextField {
                id: publicationName
                width: parent.width

                Keys.onEnterPressed: buttonSave.clicked()
                Keys.onReturnPressed: buttonSave.clicked()
            }

            Item {
                width: parent.width
                height: 15
            }

            Label {
                width: parent.width
                text: qsTr("Tipo de publicação")
                font.bold: true
            }
            ComboBox {
                id: publicationType
                width: parent.width
                currentIndex: -1

                textRole: "name"
                model: listPublicationsType
            }

            Item {
                width: parent.width
                height: 15
            }
        }
    }

    Component.onCompleted: padding = 10

    footer: DialogButtonBox {
        Button {
            text: qsTr("Cancel")
            flat: true

            onClicked: {
                dialog.close()
            }
        }
        Button {
            id: buttonSave
            text: qsTr("Save")
            flat: true

            onClicked: {
                if (publicationName.text === "") {
                    object.notification = qsTr("*É obrigatório definir o nome da publicação")
                    return
                }

                if (publicationType.currentText === "") {
                    object.notification = qsTr("*É obrigatório definir o tipo da publicação")
                    return
                }

                if (dialog.publicationId)
                    DB.update('Publications', 'id=' + dialog.publicationId, {name: publicationName.text, pubTypeId: publicationType.model.get(publicationType.currentIndex).id})
                else
                    DB.add('Publications', {name: publicationName.text, pubTypeId: publicationType.model.get(publicationType.currentIndex).id})
                listPublications.getRows()
                dialog.close()
            }
        }
    }
}
