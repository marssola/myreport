import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.4

ToolBar {
    id: toolBar

    property string title: ""
    Material.background: object.primary

    RowLayout {
        width: parent.width

        ToolButton {
            text: "\uf060"
            font.family: fa.name
            font.pixelSize: 20

            Material.foreground: "#FFF"
            onClicked: stackView.pop()
        }

        Label {
            Layout.fillWidth: true
            text: toolBar.title
            Material.foreground: "#FFF"
            font.pixelSize: 20
            elide:  Label.ElideRight
            verticalAlignment: Label.AlignBottom
        }
    }
}
