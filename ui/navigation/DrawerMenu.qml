import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.4

import "../Components"

Drawer {
    id: drawer
    width: (window.width > 500)? 360 : (window.width < 300) ? window.width - 10 : 0.75 * window.width
    height: window.height

    ColumnLayout {
        anchors.fill: parent
        spacing: 0

        Rectangle {
            Layout.fillWidth: true
            Layout.preferredHeight: 150
            color: object.primary

            RowLayout {
                anchors.fill: parent

                Rectangle {
                    Layout.preferredHeight: 100
                    Layout.preferredWidth: 100
                    Layout.leftMargin: 10
                    radius: 50
                    clip: true

                    Label {
                        anchors.fill: parent
                        text: "\uf007"
                        color: object.primary
                        font.family: fa.name
                        font.pixelSize: parent.width * 0.7

                        verticalAlignment: Label.AlignVCenter
                        horizontalAlignment: Label.AlignHCenter
                    }
                }

                Label {
                    Layout.fillWidth: true
                    Layout.rightMargin: 10
                    text: qsTr("Seu Nome")
                    Material.foreground: "#FFF"

                    font.pixelSize: 20
                    wrapMode: Text.WordWrap
                }
            }
        }

        ListModel {
            id: modelMenu

            ListElement {
                label: qsTr("Relatório")
                iconName: "\uf2bb"
                page: "qrc:/ui/pages/MainPage.qml"
            }

            ListElement {
                label: qsTr("Revisitas")
                iconName: "\uf0c0"
                page: "qrc:/ui/pages/RevisitsPage.qml"
            }

            ListElement {
                label: qsTr("Publicações")
                iconName: "\uf02d"
                page: "qrc:/ui/pages/PublicationsPage.qml"
            }

            ListElement {
                label: qsTr("Vídeos")
                iconName: "\uf008"
                page: "qrc:/ui/pages/MediaPage.qml"
            }

            ListElement {
                label: qsTr("Configurações")
                iconName: "\uf013"
                page: "qrc:/ui/pages/SettingsPage.qml"
            }

            ListElement {
                label: qsTr("Ajuda")
                iconName: "\uf128"
                page: "qrc:/ui/pages/HelpPage.qml"
            }

            ListElement {
                label: qsTr("Sobre")
                iconName: "\uf129"
                page: "qrc:/ui/pages/AboutPage.qml"
            }
        }

        ListView {
            Layout.fillWidth: true
            Layout.fillHeight: true
            clip: true

            model: modelMenu
            delegate: ItemMenu {
                width: parent.width
                text: label
                iconFont: iconName

                highlighted: object.pageActive === index
                onClicked: {
                    if (!highlighted) {
                        if (index === 0) {
                            for (var i = stackView.depth; i > 1; --i) {
                                stackView.pop(i)
                            }
                            drawer.close()
                            return
                        }
                        openPage(page)
                        object.pageActive = index
                    }
                    drawer.close()
                }
            }
        }
    }
}
