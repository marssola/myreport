import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3
import QtQuick.Controls.Material 2.4

import "../functions/Utils.js" as Utils

ToolBar {
    id: toolBar

    Material.background: object.primary

    Column {
        width: parent.width

        RowLayout {
            width: parent.width

            ToolButton {
                text: "\uf0c9"
                font.family: fa.name
                font.pixelSize: 20

                Material.foreground: "#FFF"
                onClicked: leftMenu.open()
            }

            Label {
                Layout.fillWidth: true
                Layout.fillHeight: true

                text: qsTr("Meu Relatório")
                Material.foreground: "#FFF"
                font.pixelSize: 20
                elide:  Label.ElideRight
                verticalAlignment: Label.AlignVCenter
            }

            ToolButton {
                text: "\uf133"
                font.family: fa.name
                font.pixelSize: 22

                Material.foreground: "#FFF"

                Label {
                    text: Utils.getMonth(object.date.getMonth(), 3)
                    color: object.primary
                    font.pixelSize: parent.font.pixelSize * 0.4

                    anchors.fill: parent
                    anchors.topMargin: parent.height / 2 * 0.9
                    horizontalAlignment: Label.AlignHCenter

                    z: 2
                }

                onClicked: openDialog("DialogSelectMonth")
            }

            ToolButton {
                text: "\uf1e0"
                font.family: fa.name
                font.pixelSize: 20

                Material.foreground: "#FFF"
            }

            /*ToolButton {
                text: "\uf067"
                font.family: fa.name
                font.pixelSize: 20

                Material.foreground: "#FFF"
            }*/
        }

        TabBar {
            id: tabReportType
            width: parent.width
            Material.foreground: "#FFF"
            Material.accent: "#FFF"

            currentIndex: object.swipeIndexReport
            onCurrentIndexChanged: object.swipeIndexReport = currentIndex

            TabButton {
                height: parent.height
                text: qsTr("Relatório")
            }
            TabButton {
                height: parent.height
                text: qsTr("Entrada")
            }
        }
    }
}
